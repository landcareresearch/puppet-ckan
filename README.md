# ![ckan](https://bitbucket.org/landcareresearch/puppet-ckan/downloads/logo-48x48.png) CKAN Puppet Module

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/ckan.svg)](https://forge.puppetlabs.com/landcareresearch/ckan)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetCkan_PuppetCkan%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetCkan_PuppetCkan&guest=1)
[![Project Stats](https://www.openhub.net/p/puppet-ckan/widgets/project_thin_badge.gif)](https://www.openhub.net/p/puppet-ckan)

## About

This module installs, configures, and manages ckan.
Customizations such as site logo, about page, license, and
customized css are easily configurable from this module.  
The ckan database is automatically backed up every day to
<backup_dir>/ckan_database.pg_dump.

## Installation

The module can be obtained from the [Puppet Forge](http://forge.puppetlabs.com/landcareresearch/ckan).  The easiest method for installation is to use the
[puppet-module tool](https://github.com/puppetlabs/puppet-module-tool).  Run the following command from your modulepath:

`puppet-module install landcareresearch/ckan`

## Requirements

In order to get all of the dependencies do the following:

* puppet module install landcareresearch-ckan

Other Requirements

* Ubuntu 12.04, 14.04, 16.04 Operating System.

## Administration

Once ckan is setup and installed, its necessary to manage the instance through several command line tools.

### Create Admin Account

Creating an admin account has been simplified with the use of the following command:

```/usr/local/bin/ckan_create_admin.bash <USERNAME> <EMAIL>```

### Purge Account

CKAN currently doesn't enable automation in purging users.  You can easily deactivate a user from the web ui.  There is a paster command for marking a user as 'deleted' but that user is still in the system.
In order to completely remove (purge) a user from the system, use the following command:

```/usr/local/bin/ckan_purge_account.bash <USERNAME>```

### Backups

In order to backup the entire ckan installation, the database and the file store need to be included in your backup system.
The following paths should be backed up.

* <backup_dir>/ckan_database.pg_dump
* /var/lib/ckan/default/resources
* /var/lib/ckan/default/storage

### Backup Recovery

In order to recover from a data failure or to just install a new instance do the following as root.

```/usr/local/bin/ckan_recover_db.bash```

### Upgrading ckan puppet module

Its currently unsupported to upgrade between minor versions.  For instance version 0.2.x to version 0.3.x.  Please see the change log for more details.
Its highly recommended to provision a clean system when moving between major and minor versions.  Upgrading between build versions can be done on 
existing systems.

### Upgrading CKAN

Upgrading between ckan versions is currently not fully automated via puppet.  To upgrade on an existing server, just change the ckan version.
After puppet is run with the new ckan version, there are two scripts that can be used after the upgrade.

* `/usr/local/bin/ckan_upgrade_db.bash`
* `/usr/loca/bin/ckan_upgrade_version.bash`

ckan_upgrade_db will upgrade the database schema between versions.  
ckan_upgrade_version will create the necessary views.

## Classes

See REFERENCE.md for details.

**Extensions**
The following are classes that utilize the ckan modularization framework known as [extensions](http://extensions.ckan.org).
To fully realize the plugin, puppet needs to be run twice.  The first time puppet runs,
the extension won't be added to ckan due to how this module manages the plugins.

See REFERENCE.md for details on the ext classes.

## Manual Configuration

A systems admin account is still required to be created manually.  If using Ubuntu, use the following command:

```bash
/usr/local/bin/ckan_create_admin.bash <username>
```

## Module Usage

This section shows example uses of the ckan module.

### Example 1

This example demonstrates the most basic usage of the ckan module.

```puppet
class { 'ckan':
  site_url              => 'test.ckan.com',
  site_title            => 'CKAN Test',
  site_description      => 'A shared environment for managing Data.',
  site_intro            => 'A CKAN test installation',
  site_about            => 'Pilot data catalogue and repository.',
  plugins               => 'stats text_preview recline_preview datastore resource_proxy pdf_preview',
  app_instance_id       => '{xxxxxxxxxxxxxxxx}',
  beaker_secret         => 'xxxxxxxxxxxx',
  is_ckan_from_repo     => 'false',
  ckan_package_url      => 'http://packaging.ckan.org/python-ckan_2.2_amd64.deb',
  ckan_package_filename => 'python-ckan_2.2_amd64.deb',
}
```

### Example 2

This example demonstrates a customize the ckan module.

Declaring a class that manages the configuration files.

```puppet
class {landcare_ckan::config: }
```

Declaring the ckan module with the customized parameters.

```puppet
class { 'ckan':
  site_url              => 'test.ckan.zen.landcareresearch.co.nz',
  site_title            => 'Landcare Research Test CKAN Repository',
  site_description      => 'A shared environment for managing Landcare Research Data.',
  site_intro            => 'Welcome to the Landcare Research Pilot Data Repository. This is a trial installation of the CKAN software, for us to test ahead of (all going well) a wider company rollout.',
  site_about            => 'Pilot data catalogue and repository for [Landcare Research] (http://www.landcareresearch.co.nz)',
  plugins               => 'stats text_preview recline_preview datastore resource_proxy pdf_preview',
  app_instance_id       => '{xxxxxxxxxxxxxxxx}',
  beaker_secret         => 'xxxxxxxxxxxx',
  site_logo             => $landcare_ckan::config::logo_src,
  license               => $landcare_ckan::config::license_src,
  is_ckan_from_repo     => false,
  ckan_package_url      => 'http://packaging.ckan.org/python-ckan_2.2_amd64.deb',
  ckan_package_filename => 'python-ckan_2.2_amd64.deb',
  custom_css            => $landcare_ckan::config::css_src,
  custom_imgs           => $landcare_ckan::config::custom_images_array,
  require               => Class['landcare_ckan::config'],
}
```

Class where the customized configuration files are managed

```puppet
class landcare_ckan::config {
  $img_dir             = '/usr/lib/ckan/default/src/ckan/ckan/public/base/images'
  $landcare_src        = 'puppet:///modules/landcare_ckan'
  $css_src             = "$landcare_src/custom.css"
  $background_img_src  = "$landcare_src/LCR-ckan-homepage-background.jpg"
  $custom_images_array = [$background_img_src]
  $logo_filename       = 'lcr_logo_white_sm.png'
  $logo_src            = "$landcare_src/$logo_filename"
  $license             = 'NZ_licenses_ckan.json'
  $license_src         = "$landcare_src/$license"
  $backup_dir          = '/backup',
}
```

## Deploying with Vagrant

Vagrant can be used to easily deploy the ckan module for testing or production environments.
Vagrant was used for the development of the ckan module.  

### Vagrantfile

The following content should be copied to a clean Vagrantfile. 
Note, make sure to edit puppet.module_path with a path to
where the ckan module and the ckan module dependencies are located.

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.vm.box = "precise64"
  config.vm.box = "ubuntu/precise64"

  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.provider "virtualbox" do |v| 
    v.memory = 2048
    v.cpus = 1
  end

  config.vm.provision :shell, :path => "upgrade-puppet.sh"

  config.vm.provision "puppet" do |puppet|
    puppet.module_path = "</path to modules>/modules/"
    puppet.manifests_path = "manifests"
    puppet.manifest_file  = "test-ckan.pp"
  end
end
```

### test-ckan.pp

This is the file that contains the declaration of the ckan module.
The file test-ckan.pp should be created in project_home/manifests/.

```puppet
class { 'ckan':
  site_url              => 'http://192.168.33.10',
  site_title            => 'CKAN Test',
  site_description      => 'A shared environment for managing Data.',
  site_intro            => 'A CKAN test installation',
  site_about            => 'Pilot data catalogue and repository.',
  plugins               => 'stats text_preview recline_preview datastore resource_proxy pdf_preview',
  app_instance_id       => '{xxxxxxxxxxxxxxxx}',
  beaker_secret         => 'xxxxxxxxxxxx',
  is_ckan_from_repo     => false,
  ckan_package_url      => 'http://packaging.ckan.org/python-ckan_2.2_amd64.deb',
  ckan_package_filename => 'python-ckan_2.2_amd64.deb',
  pg_hba_conf_defaults  => true,
}
```

### upgrade-puppet.sh

This file manages installing the latest puppet from puppetlabs and updates apt-get
The file upgrade-puppet.sh should be created in project_home/ (same directory as the Vagrantfile).

```bash
#!/bin/bash

DISTRIB_CODENAME=$(lsb_release --codename --short)
DEB="puppetlabs-release-${DISTRIB_CODENAME}.deb"
DEB_PROVIDES="/etc/apt/sources.list.d/puppetlabs.list" # Assume that this file's existence means we have the Puppet Labs repo added

if [ ! -e $DEB_PROVIDES ]
then
    apt-get install --yes lsb-release
    # Print statement useful for debugging, but automated runs of this will interpret any output as an error
    # print "Could not find $DEB_PROVIDES - fetching and installing $DEB"
    wget -q http://apt.puppetlabs.com/$DEB
    sudo dpkg -i $DEB
    sudo apt-get update
    sudo apt-get install --yes puppet
fi
```

## Testing

This module uses the [puppet-functional-tester](https://bitbucket.org/landcareresearch/puppet-functional-tester) for testing the supported platforms.
Please see puppet-functional-tester/README.md file additional information.

## Limitations

Only works with debian based OS's.

## Development

The module is Free and Open Source Software. Its available on bitbucket.  Please fork!
