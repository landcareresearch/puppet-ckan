# CKAN Puppet Module Changelog

## 2022-09-09 Release 6.2.20

- Added tif preview.

## 2022-08-18 Release 6.2.19

- Updated datatypes for undef default values.

## 2022-08-18 Release 6.2.18

- Added x-forwarded for logging changes.

## 2022-08-11 Release 6.2.17

- Added support for datapusher 0.0.17 and 0.0.18.
- Started adding support for datapusher_plus (not complete).

## 2022-07-21 Release 6.2.16

- Fixed datastore integration (now using its own virtual environment separate from ckan's).

## 2022-07-13 Release 6.2.15

- Added 2 new ckan configuration parameters for managing user registration.

## 2022-07-12 Release 6.2.14

- Added a missing nginx configuration parameter from large file support.

## 2022-07-12 Release 6.2.13

- Removed the parameter for large file support.
- Added individual parameters for managing timeouts replacing the large file support.

## 2022-07-08 Release 6.2.12

- Added an option to remove all forms of actions from bots.

## 2022-07-01 Release 6.2.11

- Increased timeout for large files to 5h.

## 2022-06-30 Release 6.2.10

- For large file uploads, change default settings from 50GB to 200GB.

## 2022-06-17 Release 6.2.9

- Added a new configuration parameter for setting favicon.

## 2022-06-15 Release 6.2.8

- Added a new configuration parameter for specifying facet search default value.

## 2022-06-09 Release 6.2.7

- Added parameter for adding a list of fields in the schema.
- Updated global formatting to meet new puppet lint standards.
- Replaced legacy facts with $facts variable and associated hashes.
- Changed feeds parameters to optional strings.
- Defined missing types for class parameters.

## 2022-05-31 Release 6.2.6

- Added a parameter to the spatial extension for disabling the spatial widget.

## 2022-05-31 Release 6.2.5

- Added a param for controlling if a dataset can be created without an organisation.

## 2022-04-21 Release 6.2.4

- Fixed the created admin script.

## 2022-02-15 Release 6.2.2 & 6.2.3

- Added additional parameter to language downloads.

## 2022-02-15 Release 6.2.1

- Fixed an error with selecting the python version.

## 2022-02-15 Release 6.2.0

- Zipreview's default branch changed to Develop.
- Added python 3 support.
- Added Ckan version 2.9 support.

## 2021-12-13 Release 6.1.19

- Added solr environment variable.

## 2021-11-24 Release 6.1.18

- Updated dependencies.

## 2021-11-10 Release 6.1.17

- Added support for Solr Version 6.5.0 to 6.6.6.
  Solr Version 7 and 8 do not work due to text_general field type.
- Added new parameters for GA Reports.

## 2021-09-17 Release 6.1.16

- Added flag in lcrnz extension for disabling repeating extension.
- Added a revision parameter for the spatial extension.
- Fixed a few variables that were mispelled causing warnings.
- Added additional dependancies for ldap extension.

## 2021-09-17 Release 6.1.15

- Fixed a bug in the create_admin script.
- Preparing Harvester for ckan version 2.9+
- Enabled a switch to turn off ldap from lcrnz extension.

## 2021-04-12 Release 6.1.14

- Fixed an error in the documentation causing invalid an issue when the references documentation is created.

## 2021-04-12 Release 6.1.13

- Added additional parameters to ckanext-spatial to allow for custom basemaps and attribution.

## 2021-04-07 Release 6.1.12

- Updated ckanext-scheming allowing for arbitary sources to be used instead of the default git repository.
- Cleaned up ckanext-scheming paramter list including missing documentation.
- Added ext modules to the API Reference.

## 2021-03-31 Release 6.1.11

- Added a parameter to ckanext-spatial that allows for checking the spatial field in any dictionary (not just the extra dictionary).

## 2021-03-29 Release 6.1.10

- Updated the JTL library for solr-spatial-field.
  Note, I wasn't able to get the solr-spatial-field working, so for now, follow the recomendation
  of using solr.  You can calculate the bounding box automatically with a validator.  This is demonstrated
  by the [ckanext-quavonz validator.py](https://bitbucket.org/landcareresearch/ckanext-quavonz/src/491557944f504bace81735b124615625735e1f41/ckanext/quavonz/logic/validators.py#lines-116)
- Changed the default version of geoview to the master branch.
- Added a default for map_provider that uses the default openstreetmaps removing old configuration that no longer works.
- Fixed the URL download path for jts library.
- Updated version of mmseg4j for spatial-ext to enable 'points' for spatial search.

## 2021-03-25 Release 6.1.9

- Added a parameter for ldap extension to fallback and use ckan database if auth fails ldap.

## 2021-03-22 Release 6.1.8

- Changed default version of ckanext-scheming.
- Updated create admin script to promote existing users to have admin accounts.
- Using activate when installing extensions.

## 2021-03-18 Release 6.1.7

- Fixed an issue with the no ssl version of the nginx configuration.
- Added redis server is required and has been added.
- Fixed an issue with ldap only using master branch.
- Updated dependencies.
- Migrated to puppet-functional-tester for managing the functional tests.

## 2021-02-22 Release 6.1.6

- Added a parameter to allow for large files to be uploaded into CKAN.

## 2021-02-11 Release 6.1.5

- Changed the limits on proxy timeout.
- Changed the temp directory for storing nginx body contents.
- Added parameters for using credentials to download packages for solr and ckan.

## 2021-01-11 Release 6.1.4

- Added the package python-future to be installed to fix an issue with xloader.
- Added supervisor as a default service instead as optional.

## 2020-12-07 Release 6.1.3

- Added configuration to install ckan api versions.

## 2020-12-07 Release 6.1.2

- Added a configuration setting to the ext defined type to execute python setup.py develop which is a common
  requirement for extensions.

## 2020-12-03 Release 6.1.1

- Fixed a bug installing the ldap plugin.

## 2020-12-03 Release 6.1.0

- Added support for Ubuntu 18.04 (source installs only)

## 2020-11-25 Release 6.0.7

- Changed the installation of nodejs/npm to use the defaults by the controlling puppet module.

## 2020-09-04 Release 6.0.6

- Added revision and source to the ldap module.  As there are 2 different git repositories that can be used.
  The repos are ofkn and NaturalHistoryMuseum.

## 2020-08-25 Release 6.0.5

- Added support for ckan 2.8.
- Updated the nodejs version requirements.
- Changed ldap extension source repository from okfn to NaturalHistoryMuseum.
- Removed support for Ubuntu 14.04.

### Known issues

- Datastore still has errors.
- No support for Ubuntu 18.04 as there are issues with the nodejs puppet module.

## 2020-08-11 Release 6.0.4

- Added schema for restricted scheming to fill out data creation menus.

## 2020-07-23 Release 6.0.3

- Fixed an issue with the restricted extension's schema.

## 2020-05-11 Release 6.0.2

- Updated documentation.
- server_name is now a required parameter.
- Updated vagrant testing suite.
- Apache now purges configurations not supplied by this module.
- Added xloader extension but not fully tested.
- Added a parameter for disabling datapusher for source install only.
- Added datastore configuration for source installation.

## 2020-05-11 Release 6.0.1

- Added installing by source where default was installing by package.

## 2020-04-29 Release 6.0.0

- Added a solr parameter to include the elevate.xml required for ckan and solr.
  This parameter was added in solr 0.8.0 as by default, the elevate.xml was copied over, but later version of solr do not include
  elevate.xml.  This may need to be revisted when upgrading past solr version 7.x.  Currently, solr version 5.x is recommended for ckan.
- Added composite ext (recommended for restricted ext).
- Added scheming ext (recommended for restricted ext).
- Updated to comply with Puppet Version 6.
- Replaced Anchor patter with contains.
- Changed the formatting of the contributors file.
- The restricted extension has not been verified to work correctly, so consider this extension as expiremental.

## 2020-01-23 Release 3.1.1

- Added restricted and resourceauthorizer ckan extensions.

## 2020-01-07 Release 3.1.0

- wget pupet module has been depricated, so wget puppet module has been replaced by [Archive](https://forge.puppet.com/puppet/archive).
- Changed dependency for nodejs puppet module.
- Updated vagrant development environment.
- Fixed an issue with array concatination with geoview extension.
- Bounded all dependencies in metadata file.
- Updated dependencies in the vagrant test suite.

## 2019-10-14 Release 3.0.3

- Changed from using check_run to psql command with unless clause which is more reliable.
- Added 'uploads' directory in the storage path.

## 2019-10-14 Release 3.0.2

- Changed file permissions on lang directory (a fix for 2.7.6 which seems to require writable permissions to world).
- Fixed changelog formatting to comply with markdown linting.
- Fixed readme formatting to comply with markdown linting.

## 2019-06-13 Release 3.0.1

- Added a param to block PiplBot in nginx configuration.

## 2019-01-17 Release 3.0.0

- Upgraded to latest version of postgis module.
- Added param for setting postgresql version .

## 2018-11-01 Release 2.7.9

- Fixed an issue with ext's using vcsrepo

## 2018-11-01 Release 2.7.8

- Updated license in metadata.

## 2018-11-01 Release 2.7.7

- Changed the ext to use 'latest' to pull git repositories.
- Added the requirements section in the meta data.

## 2018-09-25 Release 2.7.6

- Changed Source URL to use https protocol.
- Changed formatting in change log.

## 2018-09-25 Release 2.7.5

- Added revision parameter to harvest extension.
- Updated dcat plugin module configuration.
- Removed redis server package from required packages as it was already included in redis extension.

## 2018-06-27 Release 2.7.4

- Added parameter parsing to the ckan_create_admin script.
- Added required email parameter for ckan_create_admin script.

## 2018-06-25 Release 2.7.3

- Changed the permissions on extension source directory.
- Added types to the ckan::ext class.
- Added user parameter to the ckan::ext class to use with vcsrepo.

## 2018-06-20 Release 2.7.2

- Removed types from readme.
- Cleaned up a variable name.

## 2018-06-20 Release 2.7.1

- Changed a parameter in ext::lcrnz to be optional.

## 2018-06-20 Release 2.7.0

- Added support for SSL (nginx).
- Fixed directory permissions for language support.
- Added parameter types.
- Changed documentation style to use Puppet Strings.
- Added REFERENCE.md for class and type documentation based on Puppet Strings.
- Removed Preview loadable parameter as it was deprecated as of ckan 2.3.

## 2018-01-23 Release 2.6.10

- New version of solr puppet module which uses OpenJDK instead of oracle.

## 2018-01-17 Release 2.6.9

- Added the zip preview extension support

## 2017-12-06 Release 2.6.8

- Added support for ckan 2.7
- Added support for solr 6.5.1 which requires the ckan/lib/search/__init__.py file in order to change schema.xml to managed-schema
- Support for Ubuntu 16.04.

## 2017-09-25 Release 2.6.7

- Changed wget from a class declaration to an include statement
  to allow for wget to be used in other puppet modules.

## 2017-07-05 Release 2.6.6

- Added revision for google analytics extension.

## 2017-05-16 Release 2.6.5

- Added max_file_size parameter.
- Added activity_streams_enabled & activity_streams_email_notification parameters.
- Added tracking_enable parameter.
- Added image_formats parameter.
- Updated readme with new parameters and links to configuration documentation.

## 2017-04-28 Release 2.6.4

- Added 2 new configuration parameters.
- Deleted undef variables in ckan::params
- Removed deleted variables in init.pp and replaced with undef.

## 2017-04-10 Release 2.6.3

- ckan verison 2.6 checks the schema version in the schema.xml file. By changing the version to 2.3,
  the check schema version error is fixed.

## 2017-04-10 Release 2.6.2

- Added the ability to install languages in the default directory.

## 2017-04-5 Release 2.6.1

- Updated solr puppet module version
- Updated systemd puppet module version
- Updated postgis version for Ubuntu 16.04

## 2017-04-05 Release 2.6.1

- Added new dependencies for ckan 2.6
- Moved git-core to required packages from its own declaration in install.pp
- Added a new type called lang which allows the installation of languages.

## 2017-03-15 Release 2.6.0

- Added support for CKAN version 2.6
- Changed from python-software-properties to software-common-properties
- Changed the default URL from latest to archive for downloading solr
- Changed default version of solr to 5.5.3
- Tested with Puppet 4

## 2017-01-25 Release 2.5.1

- the pdfview ext now adds pdf_view to the default_view parameter.

## 2017-01-25 Release 2.5.0

- Added views so that officedocs extension manages both plugins and views.
- Reworked views so that 'default_views' requires an array not a string.
- Fixed issues with geoview related to default_views

## 2017-01-23 Release 2.4.2

- Added the office docs extension.
- Fixed an issue with galleries ext not installing properly.

## 2016-12-23 Release 2.4.1

- Added a dependancy to puppetlabs/inifile

## 2016-12-20 Release 2.4.0

- Removed datastore extension.  Just set datastore as a plugin as there is no extra work required to enable datastore.
- Fixed a spelling error in the hide_overlays parameter in geoview extension.
- Replaced puppet template code with the puppetlabs inifile types.
- Converted all extensions to use the new inifile puppet module
- Removed urls that use the git protocol and changed to http protocol.
- Fixed a bug with ckan::pip_package anchor naming
- Added a new parameter for setting the roles cascading.

## 2016-11-29 Release 2.3.3

- Custom facts have been used to auto populate the ckan.plugins configuration parameter based on ckan::ext.  However, in later version of factor, this seems to be
  unreliable especially when running from Vagrant. This version removes custom facts completely.  The mechanism still uses the /etc/ckan/plugins.out file for collecting
  the plugins and requires 2 puppet runs to enable the plugins in the ckan service.  However, this solution will work consistently.

## 2016-11-18 Release 2.3.2

- The latest version of the postgresql puppet module 4.8.0 has changed their internal dependency structure with the apt puppet module.  This has
  caused the dependency cycle issue because there was dependencies between the landcareresearch/solr module and eventually the puppetlabs/postgresql module.
  This build decouples those two modules completely.

## 2016-11-02 Release 2.3.1

- Added new dependencies based on updated version of solr puppet module
  including systemd, and java8

## 2016-11-02 Release 2.3.0

- Fixed potential dependency cycle with newer versions of postgresql puppet module.
- Upgraded the solr module version to 0.5.x. Do not use version
  2.3.0 with existing ckan instances as this will most likely cause errors
  with the solr installation.  the Solr 0.5.x puppet module now 
  manages solr uses tarballs and not packages.  This is an improvement
  but means that legacy systems will have problems upgrading.

## 2016-11-02 Release 2.2.7

- Added a cron job for to ensure harvest runs periodically.
- Updated docs to reflect additional parameters on the harvest ext.

## 2016-07-23 Release 2.2.6

- Added parameters for managing the smtp service.
  Note, this does NOT install an smtp service.

## 2016-07-15 Release 2.2.5

- Fixed an error in the spatial ext template.

## 2016-07-15 Release 2.2.4

- Mapquest is no longer serving tiles via the api used by spatial extension.  So a feature has been added to use open street map tiles.
- Discovered that galleries extension doesn't install properly on Ubuntu 12.04 so updated documentation.

## 2016-07-11 Release 2.2.3

- Added the galleries extension
- Added a global parameter for specifying the ckan packaged python library

## 2016-07-07 Release 2.2.2

- Added a new class for installing redis for the harvest ext.
- Added missing ext classes documentation from readme.
- Added links to each ext class github repo.
- Removed Authors field in source file headers (as this promotes more of a community).  Using contributors.txt is a better way.

## 2016-07-06 Release 2.2.1

- Fixed a typo in a require statement
- Fixed geoview ext's configuration providing order and white space.

## 2016-06-23 Release 2.2.0

- Added a parameter for the sender email.
- Added a parameter for the receiver email.
- Added a missing file source.
- Added a missing template.
- Updated documentation for modules.
- Updated readme (partial).

## 2016-06-22 Release 2.1.15

- Added a parameter for managing the directory for database backups and restores.
- Fixed a configuration problem with spatial search.
- Fixed a bug where an exec runs every puppet run (wsgi_create).
- Fixed a bug for the location of the paster command.

## 2016-06-21 Release 2.1.14

- Added geoview extension

## 2016-05-03 Release 2.1.13

- Fixed spatial search problem with templates.

## 2016-04-27 Release 2.1.12

- Changed from using the ckan python pip to the system packaged pip.
- Added installation of python2.7 and python-pip for deb versions.

## 2016-04-27 Release 2.1.11

- Added a parameter to change the postgis version.

## 2016-04-22 Release 2.1.10

- Added support for ckan version 2.5.

## 2016-04-18 Release 2.1.9

- A bug was discovered with the automatic backups.  If the spatial extension was enabled than backups would fail.  This bug has been fixed.

## 2016-03-29 Release 2.1.8

- Added a new extension called private dataset
- Added vagrant testing environment
- Handling nodejs for Ubuntu 12.04 and 14.04

## 2016-01-11 Release 2.1.7

- Added parameters to change the source repo for the lcr extension.

## 2015-12-09 Release 2.1.6

- Added ckan_upgrade_db.bash
- Added ckan_create_views.bash
- Added ckan_purge_account.bash (uses expect package)
- Added recaptcha version.  Changed the default to 2 instead of 1.
- Fixed postgresql ppa problem bug.
- Added Ubuntu 14.04 support.
- Updated the solr default version to 5.3.0

## 2015-10-30 Release 2.1.4

- Added the json_formats and xml_formats for the configuration file.
- Text Formats will always be added to configuration.
- Changed recaptcha public/private defaults to undef.
- Updated for future parser compliance (puppet 4)

## 2015-09-xx Release 2.1.0

- Added support for ckan version 2.3
- Added default_locale param for ckan configuration
- Added ckan.i18n_directory param for ckan configuration
- Added support for 2.3 changes like preview vrs new view system
- Upgraded Landcare Research ext to lcrnz
- Added repeating extension
- Added ldap extension
- Enabled the storage path of the file store to be set by module.
- Updated to using Solr 5.x
- Enabled both solr and solr-spatial-field for the backend to spatiel search.

## 2015-04-02 Releaes 2.0.1

- The changed to using the postgresql password hash function.
- Added ckan version specifing the version of ckan to use.

## 2015-03-23 Releaes 2.0.0

### NOTE

Due to a change of how the database is initialized, upgrading from previous version of this module will
re-initialize your database!!!!  This means, you need to backup the database before doing this upgrade.

To recover your database use the following command (make sure you sudo su first)

```bash
paster --plugin=ckan db clean --config=/etc/ckan/default/production.ini ckan_database.pg_dump
paster --plugin=ckan db load --config=/etc/ckan/default/production.ini ckan_database.pg_dump
```

- Added the landcareresearch/solr puppet module.
- Tested with solr 4.10.3.
- Database initialization is now moved to a bash script in /usr/local/bin
- Added a solr schema parameter that instructs which schema version to use.

## 2015-03-04 Release 1.0.13

- Migrated from github to bitbucket
- Changed ownership of puppetforge account
- Added openhub badge to readme.
- Migrated changelog into md format.
- Changed ckan_run dependancy to landcareresearch account.
- Output of new CKAN Fact now correctly reports if the /etc/ckan/plugins.out file doesn't exist.

## 2015-02-16 Version 1.0.12

- Including extension plugins in the plugins parameter caused the module to fail due to a depedancy on the extension to already be installed prior to ckan started.
- This patch incorperates the plugin into the extension so the extension plugins no longer need to be set via the plugin option.
  It will require 2 puppet runs in order to bring the extensions online though (there isn't any way around this that I know of).

## 2015-01-29 Version 1.0.11

- Added ckan user password string to ckan.ini.
- Added puppetforge badge

## 2015-01-28 Version 1.0.10

- Added an option to enable event tracking for the Google Analystics which pushes events every hour
- Added a convience script for creating admin accounts.
- Cleaned up code
- Added documentation for ckan::config
- Added configuration for backup directory
- Added configuration for ckan database password

## 2015-01-14 Version 1.0.9

- The ckanapi can now be installed (optional)
- A helper script for calling ckanapi command line tool

## 2015-01-08 Version 1.0.8

- Cleaned up the code based on puppet-lint (via puppetlinter.com)
- Enabled github hook for puppetlinter so future commits are checked and reported

## 2015-01-07 Version 1.0.7

- Fixed a recent security exploit that has effected CKAN sites globally
- Set the security settings to restrict anonymous users from creating groups and datasets
- Set the default backend for spatial search to solr in the spatial search extension
- Changed backups from weekly to daily

## 2014-11-12 Version 1.0.6

- CKAN Developers have added a new submodule for managing CKAN extensions.
- Added ckan::ext submodule
- Added Google Analytics extension
- Added Hierarchy extesion
- Added New Zealand Landcare extension
- Added Spatial extension
- Updated to using metadata.json

## 2014-08-13 Version 1.0.5

- A CKAN Developer has cleaned up the module and prepared for ckan extensions.
- Removed apache module dependency
- Removed module stage complexity with anchor pattern
- Removed hard coded security keys from production.ini.erb
- Added security keys to module parameters

## 2014-07-23 Version 1.0.4

- Removed the reset_apt module as vagrant can handle updating puppet with a recomended script
- Updated to the latest apache module which has the necessary changes integrated
- Changed default value of the apache headers variable

## 2014-06-09 Version 1.0.3

- Added dependancy for Debian/Ubuntu based systems only
- Updated readme with new installation parameters
- Added Server admin's email specified
- Added recaptcha support
- Added max_resource_size parameter
- Added data pusher formats
- Added apache head configuration (in order to control search engine crawlers) and is optional
- Added the postgres password as a parameter
- Added postgres hba configuration to pass in as a parameter

## 2014-06-08 Version 1.0.2

- Fixed a bug if the license was left off, caused errors in datastore
- Added support for CKAN 2.2 package

## 2014-01-15 Version 1.0.1

- Added a parameter to disable the apt reset
- Removed puppetlabs-apt and apt::ppa which was causing dependency loops if a class outside of ckan required apt::ppa
- Removed ppa for nodejs & ubuntugis
- Added puppetlabs-nodejs class
- Added dependency for puppetlabs-nodejs

## Version 1.0.0

- Initial Release.
