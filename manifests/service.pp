# @summary Manages services for ckan
#
# @see http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
class ckan::service {
  exec { 'restart solr':
    command     => '/usr/sbin/service solr restart',
    refreshonly => true,
    subscribe   => Class['ckan::conf::production'],
    #notify      => Service['nginx','httpd'],
  }

  service { $ckan::apache_service:
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require    => Exec['restart solr'],
  }
  service { 'nginx':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    #require    => Exec['restart solr'],
    require    => Service[$ckan::apache_service],
  }
}
