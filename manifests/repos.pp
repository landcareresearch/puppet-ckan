# @summary repos for ckan install
#
class ckan::repos {
  package { 'python-software-properties':
    ensure => installed,
  }

  file { $ckan::ckan_package_dir:
    ensure => directory,
  }

  # downloads the package from the specified source.
  if $ckan::is_ckan_from_repo == false {
    archive { "${ckan::ckan_package_dir}/${ckan::ckan_package_filename}":
      source  => $ckan::ckan_package_url,
      require => File[$ckan::ckan_package_dir],
    }
  }
}
