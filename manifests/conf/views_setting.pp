# @summary Manages a ini_subsetting for the views
#
# @param ensure
#   present if the init setting should be included and absent if it should be removed.
#
# @param path
#   The path to the settings file.
#
# @param section
#   The section in the ini the setting belongs to.
#
# @param key_val_separator
#   The seperator between the key and value.
#
# @param setting
#   The setting to write to.
#
# @param subsetting
#   The name of the setting.
#
# @param use_exact_match
#  Set to true if the subsettings don't have values and you want to use exact matches to determine if the subsetting exists.
#
define ckan::conf::views_setting (
  String  $ensure            = present,
  String  $path              = $ckan::ckan_conf,
  String  $section           = 'app:main',
  String  $key_val_separator = '=',
  String  $setting           = 'ckan.views.default_views',
  String  $subsetting        = $title,
  Boolean $use_exact_match   = true,
) {
  ini_subsetting { "${title}_views":
    ensure            => $ensure,
    path              => $path,
    section           => $section,
    key_val_separator => $key_val_separator,
    setting           => $setting,
    subsetting        => $subsetting,
    use_exact_match   => $use_exact_match,
  }
}
