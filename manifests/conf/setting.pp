# @summary Manages a ini_setting for the general ckan configuration.
#
# @param value
#   The value of the setting.
#
# @param ensure
#   present if the init setting should be included and absent if it should be removed.
#
# @param path
#   The path to the settings file.
#
# @param section
#   The section in the ini the setting belongs to.
#
# @param key_val_separator
#   The seperator between the key and value.
#
# @param setting
#   The setting to write to.
#
# @param use_exact_match
#  Set to true if the subsettings don't have values and you want to use exact matches to determine if the subsetting exists.
#
#
define ckan::conf::setting (
  Optional $value             = undef,
  String   $ensure            = present,
  String   $path              = $ckan::ckan_conf,
  String   $section           = 'app:main',
  String   $key_val_separator = '=',
  String   $setting           = $title,
  Boolean  $use_exact_match   = true,
) {
  ini_setting { "${title}_setting":
    ensure            => $ensure,
    path              => $path,
    section           => $section,
    key_val_separator => $key_val_separator,
    setting           => $setting,
    value             => $value,
  }
}
