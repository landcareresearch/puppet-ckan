# @summary Manages the production.ini file that controls ckan.
#
class ckan::conf::production {
  # == vars == #
  $default_section            = 'DEFAULT'
  $main_section               = 'app:main'
  $server_main_section        = 'server:main'
  $loggers_section            = 'loggers'
  $logger_root_section        = 'logger_root'
  $logger_ckan_section        = 'logger_ckan'
  $logger_ckanext_section     = 'logger_ckanext'
  $handlers_section           = 'handlers'
  $handlers_console_section   = 'handler_console'
  $formatters_section         = 'formatters'
  $formatters_generic_section = 'formatter_generic'

  ini_setting { 'debug':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $default_section,
    key_val_separator => '=',
    setting           => 'debug',
    value             => 'false',
  }

  # server main
  ini_setting { 'use':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $server_main_section,
    key_val_separator => '=',
    setting           => 'use',
    value             => 'egg:Paste#http',
  }
  ini_setting { 'host':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $server_main_section,
    key_val_separator => '=',
    setting           => 'host',
    value             => '0.0.0.0',
  }
  ini_setting { 'port':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $server_main_section,
    key_val_separator => '=',
    setting           => 'port',
    value             => '5000',
  }

  # app::main section
  ini_setting { 'app_use':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'use',
    value             => 'egg:ckan',
  }
  ini_setting { 'full_stack':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'full_stack',
    value             => 'true',
  }
  ini_setting { 'cache_dir':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'cache_dir',
    value             => '/tmp/%(ckan.site_id)s/',
  }
  ini_setting { 'beaker_session':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'beaker.session.key',
    value             => 'ckan',
  }

  ini_setting { 'beaker':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'beaker.session.secret',
    value             => $ckan::beaker_secret,
  }

  ini_setting { 'app_instance':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'app_instance_uuid',
    value             => $ckan::app_instance_id,
  }

  ini_setting { 'who.config_file':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'who.config_file',
    value             => '%(here)s/who.ini',
  }

  ini_setting { 'who.log_level':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'who.log_level',
    value             => 'warning',
  }

  ini_setting { 'who.log_file':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'who.log_file',
    value             => '%(cache_dir)s/who_log.ini',
  }

  # database settings
  ini_setting { 'sql':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'sqlalchemy.url',
    value             => "postgresql://ckan_default:${ckan::ckan_pass}@localhost/ckan_default",
  }

## DataStore Extension
  ini_setting { 'datastore_write':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.datastore.write_url',
    value             => "postgresql://ckan_default:${ckan::ckan_pass}@localhost/datastore_default",
  }

  ini_setting { 'datastore_read':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.datastore.read_url',
    value             => "postgresql://datastore_default:${ckan::ckan_pass}@localhost/datastore_default",
  }
  ini_setting { 'default_fts_lang':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.datastore.default_fts_lang',
    value             => 'english',
  }
  ini_setting { 'default_fts_index_method':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.datastore.default_index_method',
    value             => 'gist',
  }

  ini_setting { 'datapusher_url':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.datapusher.url',
    value             => 'http://0.0.0.0:8800',
  }
  ini_setting { 'datapusher_formats':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.datapusher.formats',
    value             => $ckan::datapusher_formats,
  }

  ini_setting { 'site_url':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.site_url',
    value             => $ckan::site_url,
  }

  ini_setting { 'anon_create_dataset':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.anon_create_dataset',
    value             => 'false',
  }

  ini_setting { 'create_unowned_dataset':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.create_unowned_dataset',
    value             => $ckan::create_unowned_dataset,
  }

  ini_setting { 'create_dataset_if':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.create_dataset_if_not_in_organization',
    value             => 'false',
  }

  ini_setting { 'create_groups':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.user_create_groups',
    value             => 'false',
  }

  ini_setting { 'create_organizations':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.user_create_organizations',
    value             => 'false',
  }

  ini_setting { 'delete_groups':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.user_delete_groups',
    value             => 'true',
  }

  ini_setting { 'delete_organizations':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.user_delete_organizations',
    value             => 'true',
  }

  ini_setting { 'create_user_via_api':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.create_user_via_api',
    value             => $ckan::create_user_via_api,
  }

  ini_setting { 'create_user_via_web':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.auth.create_user_via_web',
    value             => $ckan::create_user_via_web,
  }

  ini_setting { 'site_id':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.site_id',
    value             => 'ckan',
  }

  ini_setting { 'solr_url':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'solr_url',
    value             => 'http://127.0.0.1:8983/solr/ckan',
  }

  # instruct ckan to use Solr
  ini_setting { 'simple_search':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.simple_search',
    value             => false,
  }

  # update plugins with ini
  if $ckan::plugins {
    ensure_resource(ckan::conf::plugin_setting,$ckan::plugins)
  }

  if $ckan::default_views {
    ensure_resource(ckan::conf::views_setting,$ckan::default_views)
  }

  # Front-End Settings
  ini_setting { 'extra':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'extra_public_paths',
    value             => '/var/local/ckan',
  }

  ini_setting { 'site_title':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.site_title',
    value             => $ckan::site_title,
  }

  # logo
  if $ckan::site_logo != '' {
    ini_setting { 'site_logo':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.site_logo',
      value             => '/base/images/site_logo.png',
    }
  }

  ini_setting { 'site_description':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.site_description',
    value             => $ckan::site_description,
  }

  ini_setting { 'favicon':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.favicon',
    value             => $ckan::favicon,
  }

  ini_setting { 'gravatar':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.gravatar_default',
    value             => 'identicon',
  }

  ini_setting { 'preview_direct':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.preview.direct',
    value             => 'png jpg gif',
  }

  ini_setting { 'preview_loadable':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.preview.loadable',
    value             => 'html htm rdf+xml owl+xml xml n3 n-triples turtle plain atom csv tsv rss txt json',
  }

  ini_setting { 'intro_text':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.site_intro_text',
    value             => $ckan::site_intro,
  }

  ini_setting { 'site_about':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.site_about',
    value             => $ckan::site_about,
  }

  if $ckan::custom_css != 'main.css' {
    ini_setting { 'main_css':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.main_css',
      value             => '/base/css/custom.css',
    }
  } else {
    ini_setting { 'main_css':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.main_css',
      value             => '/base/css/main.css',
    }
  }

  ini_setting { 'text_formats':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.preview.text_formats',
    value             => $ckan::text_formats,
  }

  ini_setting { 'json_formats':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.preview_json_formats',
    value             => $ckan::json_formats,
  }

  ini_setting { 'xml_formats':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.preview_xml_formats',
    value             => $ckan::xml_formats,
  }

  ini_setting { 'image_formats':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.preview_image_formats',
    value             => $ckan::image_formats,
  }

  if $ckan::recaptcha_publickey and $ckan::recaptcha_privatekey {
    ini_setting { 'recaptcha_version':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.recaptcha.version',
      value             => $ckan::recaptcha_version,
    }

    ini_setting { 'recaptcha_publickey':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.recaptcha.publickey',
      value             => $ckan::recaptcha_publickey,
    }

    ini_setting { 'recaptcha_privatekey':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.recaptcha.privatekey',
      value             => $ckan::recaptcha_privatekey,
    }
  }

  if $ckan::license != '' {
    ini_setting { 'license':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'licenses_group_url',
      value             => "file://${ckan::ckan_default}/${ckan::license_file}",
    }
  } else {
    ini_setting { 'license':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'licenses_group_url',
      value             => 'http://licenses.opendefinition.org/licenses/groups/ckan.json',
    }
  }

  # Time/Date
  ini_setting { 'display_timezone':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.display_timezone',
    value             => $ckan::display_timezone,
  }

  # Internationalisation Settings
  ini_setting { 'locale_default':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.locale_default',
    value             => $ckan::locale_default,
  }

  ini_setting { 'locales_offered':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.locales_offered',
    value             => $ckan::locales_offered,
  }

  if $ckan::i18n_directory {
    ini_setting { 'i18n_directory':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.i18n_directory',
      value             => $ckan::i18n_directory,
    }
  }

  ## Feeds Settings
  if $ckan::feeds_author_name {
    ini_setting { 'author_name':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.feeds.author_name',
      value             => $ckan::feeds_author_name,
    }
  }

  if $ckan::feeds_author_link {
    ini_setting { 'author_link':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.feeds.author_link',
      value             => $ckan::feeds_author_link,
    }
  }

  if $ckan::feeds_authority_name {
    ini_setting { 'authority_name':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.feeds.authority_name',
      value             => $ckan::feeds_authority_name,
    }
  }

  if $ckan::feeds_date {
    ini_setting { 'feeds_date':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.feeds.date',
      value             => $ckan::feeds_date,
    }
  }

  ## Storage Settings
  # Local file storage:
  ini_setting { 'storage_dir':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ofs.storage_dir',
    value             => $ckan::ckan_storage_path,
  }

  ## 2.2 config
  ini_setting { 'storage_path':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.storage_path',
    value             => $ckan::ckan_storage_path,
  }

  ini_setting { 'max_resource_size':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.max_resource_size',
    value             => $ckan::max_resource_size,
  }

  ini_setting { 'max_image_size':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.max_image_size',
    value             => $ckan::max_image_size,
  }

  ini_setting { 'max_file_size':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.resource_proxy.max_file_size',
    value             => $ckan::max_file_size,
  }

  ## Email settings
  if $ckan::smtp_server {
    ini_setting { 'smtp_server':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'smtp.server',
      value             => $ckan::smtp_server,
    }
  }

  if $ckan::smtp_starttls {
    ini_setting { 'smtp_starttls':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'smtp.starttls',
      value             => $ckan::smtp_starttls,
    }
  }

  if $ckan::smtp_user {
    ini_setting { 'smtp_user':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'smtp.user',
      value             => $ckan::smtp_user,
    }
  }

  if $ckan::smtp_password {
    ini_setting { 'smtp_password':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'smtp.password',
      value             => $ckan::smtp_password,
    }
  }

  if $ckan::smtp_password {
    ini_setting { 'smtp_password':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'smtp.password',
      value             => $ckan::smtp_password,
    }
  }

  if $ckan::smtp_mail_from {
    ini_setting { 'smtp_mail_from':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'smtp.mail_from',
      value             => $ckan::smtp_mail_from,
    }
  }

  if $ckan::email_errors_to {
    ini_setting { 'email_to':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'email_to',
      value             => $ckan::email_errors_to,
    }
  }

  if $ckan::email_errors_from {
    ini_setting { 'email_from':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'email_from',
      value             => $ckan::email_errors_from,
    }
  }

  if $ckan::roles_cascade {
    ini_setting { 'roles_cascade':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.auth.roles_that_cascade_to_sub_groups',
      value             => $ckan::roles_cascade,
    }
  }

  if $ckan::featured_orgs {
    ini_setting { 'featured_orgs':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.featured_orgs',
      value             => $ckan::featured_orgs,
    }
  }

  if $ckan::featured_groups {
    ini_setting { 'featured_groups':
      ensure            => present,
      path              => $ckan::ckan_conf,
      section           => $main_section,
      key_val_separator => '=',
      setting           => 'ckan.featured_groups',
      value             => $ckan::featured_groups,
    }
  }

  ini_setting { 'activity_streams_enabled':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.activity_streams_enabled',
    value             => $ckan::activity_streams_enabled,
  }

  ini_setting { 'activity_streams_email_notifications':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.activity_streams_email_notifications',
    value             => $ckan::activity_streams_email_notifications,
  }

  ini_setting { 'hide_activity':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.hide_activity_from_users',
    value             => '%(ckan.site_id)s',
  }

  ini_setting { 'tracking_enabled':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'ckan.tracking_enabled',
    value             => $ckan::tracking_enabled,
  }

  # search
  ini_setting { 'search_facets_default':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $main_section,
    key_val_separator => '=',
    setting           => 'search.facets.default',
    value             => $ckan::search_facets_default,
  }

  # loggers
  ini_setting { 'loggers_keys':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $loggers_section,
    key_val_separator => '=',
    setting           => 'keys',
    value             => 'root, ckan, ckanext',
  }
  # logger_root
  ini_setting { 'logger_root':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_root_section,
    key_val_separator => '=',
    setting           => 'level',
    value             => 'WARNING',
  }
  ini_setting { 'logger_handlers':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_root_section,
    key_val_separator => '=',
    setting           => 'handlers',
    value             => 'console',
  }
  # logger_ckan
  ini_setting { 'logger_ckan_level':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckan_section,
    key_val_separator => '=',
    setting           => 'level',
    value             => 'INFO',
  }
  ini_setting { 'logger_ckan_handlers':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckan_section,
    key_val_separator => '=',
    setting           => 'handlers',
    value             => 'console',
  }
  ini_setting { 'logger_ckan_qualname':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckan_section,
    key_val_separator => '=',
    setting           => 'qualname',
    value             => 'ckan',
  }
  ini_setting { 'logger_ckan_propagate':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckan_section,
    key_val_separator => '=',
    setting           => 'propagate',
    value             => '0',
  }
  # logger_ckanext
  ini_setting { 'logger_ckanext_level':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckanext_section,
    key_val_separator => '=',
    setting           => 'level',
    value             => 'DEBUG',
  }
  ini_setting { 'logger_ckanext_handlers':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckanext_section,
    key_val_separator => '=',
    setting           => 'handlers',
    value             => 'console',
  }
  ini_setting { 'logger_ckanext_qualname':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckanext_section,
    key_val_separator => '=',
    setting           => 'qualname',
    value             => 'ckanext',
  }
  ini_setting { 'logger_ckanext_propagate':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $logger_ckanext_section,
    key_val_separator => '=',
    setting           => 'propagate',
    value             => '0',
  }

  # handlers
  ini_setting { 'handlers_keys':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $handlers_section,
    key_val_separator => '=',
    setting           => 'keys',
    value             => 'console',
  }

  # handler console
  ini_setting { 'handler_console_class':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $handlers_console_section,
    key_val_separator => '=',
    setting           => 'class',
    value             => 'StreamHandler',
  }
  ini_setting { 'handler_console_args':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $handlers_console_section,
    key_val_separator => '=',
    setting           => 'args',
    value             => '(sys.stderr,)',
  }
  ini_setting { 'handler_console_level':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $handlers_console_section,
    key_val_separator => '=',
    setting           => 'level',
    value             => 'NOTSET',
  }
  ini_setting { 'handler_console_formatter':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $handlers_console_section,
    key_val_separator => '=',
    setting           => 'formatter',
    value             => 'generic',
  }
  # formatters
  ini_setting { 'formatters_keys':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $formatters_section,
    key_val_separator => '=',
    setting           => 'keys',
    value             => 'generic',
  }
  # formatter generic
  ini_setting { 'formatters_generic':
    ensure            => present,
    path              => $ckan::ckan_conf,
    section           => $formatters_generic_section,
    key_val_separator => '=',
    setting           => 'format',
    value             => '%(asctime)s %(levelname)-5.5s [%(name)s] %(message)s',
  }
}
