# @summary Adds a 'program' (job/task) managed by supervisor
#
# @param name
#   Arbitrary string, will be the name of the 'program' in supervisor, i.e.
#    the name given to supervisorctl (start|stop|retart|status) <name>
#
# @param command
#   The fully path/arguments of the command that is the program to run
#
# @example
#  include ckan::supervisor
#  ckan::supervisor::program { 'harvester_fetch_consumer':
#    command => "/usr/lib/ckan/default/bin/paster\
# --plugin=ckanext-harvest harvester fetch_consumer\
# --config=${ckan::ckan_conf}",
#  }
#
define ckan::supervisor::program (
  String $command
) {
  file { "/etc/supervisor/conf.d/${name}.conf":
    content => template('ckan/etc/supervisor/conf.d/program.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Exec['supervisor-update'],
    require => Package['supervisor'],
  }
}
