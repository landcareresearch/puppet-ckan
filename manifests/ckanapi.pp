# @summary Installs the ckan commandline api
#
# @see https://github.com/ckan/ckanapi
#
# Additional features:
#
# * Installs a helper script in /usr/bin/ckan/ckanapi.bash
#   which can be used to call ckanapi directly.
#
class ckan::ckanapi {
  $extdir = '/usr/lib/ckan/default/src/ckanapi'
  vcsrepo { $extdir:
    ensure   => 'present',
    provider => 'git',
    source   => 'http://github.com/ckan/ckanapi',
    revision => "ckanapi-${ckan::ckan_api_version}",
  }
  exec { 'install ckanapi requirements':
    command     => "${ckan::pip} install -r '${extdir}/requirements.txt'",
    onlyif      => "/usr/bin/test -e '${extdir}/requirements.txt'",
    refreshonly => true,
    subscribe   => Vcsrepo[$extdir],
  }
  exec { 'install ckanapi':
    command     => "${ckan::python} setup.py install",
    cwd         => $extdir,
    refreshonly => true,
    subscribe   => Exec['install ckanapi requirements'],
  }
  file { '/usr/local/bin/ckanapi.bash':
    ensure  => file,
    source  => 'puppet:///modules/ckan/ckanapi.bash',
    mode    => '0755',
    require => Exec['install ckanapi'],
  }
}
