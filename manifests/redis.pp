# @summary A simple non-configured redis server install for
#          the harvest extension.
#
# There's too many redis modules in the forge to make a good assessment or to
# enforce the use of any particular one of them for CKAN.  Enhancement
# possibilities exist in making this optional in the harvest module
# (e.g. supply your own).
#
# @example include ckan::redis
#
class ckan::redis {
  # Redis is a required package and is handled in the params.
  # Additional configuration.
  file { '/var/run/redis':
    ensure  => directory,
    owner   => 'redis',
    group   => 'redis',
    require => Package['redis-server'],
  }
}
