# @summary Very simple (barely there) installation of supervisor.
#
# This is used to run/manage background permanent tasks, e.g. for harvester jobs etc.
#
# See also ckan::supervisor::program for deploying programs run by supervisor
#
# And some use of ckan::supervisor::program (see that define for details)
#
class ckan::supervisor {
  package { 'supervisor':
    ensure  => 'present',
  }

  service { 'supervisor':
    ensure  => running,
    require => Package['supervisor'],
  }

  file { '/etc/supervisor/conf.d/supervisor-ckan-worker.conf':
    ensure  => file,
    source  => 'puppet:///modules/ckan/supervisor-ckan-worker.conf',
    require => Service['supervisor'],
  }

  exec { 'supervisor-update':
    command     => '/usr/bin/supervisorctl update',
    refreshonly => true,
    subscribe   => File['/etc/supervisor/conf.d/supervisor-ckan-worker.conf'],
  }
}
