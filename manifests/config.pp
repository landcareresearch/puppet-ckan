# @summary Configuration supporting ckan
# details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
# @param backup_dir
#   The location where backups are stored.
#
# @param [Optional[String]] site_logo
#   This sets the logo use din the title bar.
#   Default: undef
#
class ckan::config (
  String $backup_dir,
  Optional[String] $site_logo = undef,
) {
  # Variables
  $ckan_data_dir = ['/var/lib/ckan',$ckan::ckan_storage_path,"${ckan::ckan_storage_path}/uploads"]

  # add the logo but its set via the web ui and also set via production.ini
  # however, I'm not certain that the production.ini has any effect...
  if $site_logo {
    file { "${ckan::ckan_img_dir}/site_logo.png":
      ensure => file,
      source => $site_logo,
      before => File[$ckan_data_dir],
    }
  }

  file { $ckan_data_dir:
    ensure => directory,
    owner  => www-data,
    group  => www-data,
    mode   => '0755',
  }

  file { "${ckan::ckan_default}/${ckan::license_file}":
    ensure  => file,
    source  => $ckan::license,
    mode    => '0644',
    require => File[$ckan_data_dir],
    before  => File['/usr/local/bin/ckan_create_admin.bash'],
  }

  if $ckan::custom_imgs {
    # manage the default image directory
    ckan::custom_images { $ckan::custom_imgs:
      require => File[$ckan_data_dir],
      before  => File['/usr/local/bin/ckan_create_admin.bash'],
    }
  }

  # download custom css if specified
  if $ckan::custom_css != 'main.css' {
    file { "${ckan::ckan_css_dir}/custom.css":
      ensure  => file,
      source  => $ckan::custom_css,
      require => File[$ckan_data_dir],
      before  => File['/usr/local/bin/ckan_create_admin.bash'],
    }
  }

  # backup configuration
  if $ckan::enable_backup {
    if $ckan::backup_daily {
      $weekday = 'absent'
    } else {
      $weekday = '7'
    }
    file { $backup_dir:
      ensure  => directory,
      owner   => backup,
      group   => backup,
      mode    => '0755',
      require => File[$ckan_data_dir],
    }
    file { '/usr/local/bin/ckan_backup.bash':
      ensure  => file,
      content => template('ckan/ckan_backup.bash.erb'),
      mode    => '0755',
      require => File[$backup_dir],
    }
    file { '/usr/local/bin/ckan_recover_db.bash':
      ensure  => file,
      content => template('ckan/ckan_recover_db.bash.erb'),
      mode    => '0755',
      require => File['/usr/local/bin/ckan_backup.bash'],
    }
    # backup database either every day or every week at 5 am
    cron { 'ckan_backup':
      command => '/usr/local/bin/ckan_backup.bash',
      user    => backup,
      minute  => '0',
      hour    => '5',
      weekday => $weekday,
      require => File['/usr/local/bin/ckan_recover_db.bash'],
      before  => File['/usr/local/bin/ckan_create_admin.bash'],
    }
  }

  # additional userful scripts
  file { '/usr/local/bin/ckan_create_admin.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_create_admin.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File[$ckan_data_dir],
  }
  file { '/usr/local/bin/ckan_delete_account.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_delete_account.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_create_admin.bash'],
  }
  file { '/usr/local/bin/ckan_purge_account.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_purge_account.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_delete_account.bash'],
  }
  file { '/usr/local/bin/ckan_upgrade_db.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_upgrade_db.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_purge_account.bash'],
  }
  file { '/usr/local/bin/ckan_init_db.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_init_db.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_upgrade_db.bash'],
  }
  file { '/usr/local/bin/ckan_create_views.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_create_views.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_init_db.bash'],
  }
  file { '/usr/local/bin/ckan_activate_exec.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_activate_exec.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_init_db.bash'],
  }
  file { '/usr/local/bin/ckan_set_db_permissions.bash':
    ensure  => file,
    content => epp('ckan/scripts/ckan_set_db_permissions.bash.epp', { python_version => $ckan::python_version }),
    mode    => '0755',
    require => File['/usr/local/bin/ckan_activate_exec.bash'],
  }
}
