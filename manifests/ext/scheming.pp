# @summary Installs the scheming extension.
#
# @see https://github.com/ckan/ckanext-scheming
#
# @param source
#   The URL of the remote VCS repository. If undef, uses
#   "http://github.com/ckan/ckanext-scheming".
#
# @param revision
#   The tag or branch to use for the scheming extension.
#
# @param plugins
#   An array of strings that are plugins to add to the
#   plugin configuration in the production.ini.
#   @example ['scheming_datasets','scheming_groups','scheming_orginizations']
#
# @param data_schemas
#   The relative path based on a module or a full path that contains the dataset schema.  Can be json or yaml.
#   @example 'ckanext.scheming:/usr/lib/ckan/default/src/ckanext-quavonz/ckanext/quavonz/schema/dataset.yml' 
#
# @param presets
#   The relative path based on a module or a full path that contains the preset definitions.  Can be json or yaml.
#
# @param dataset_fallback
#   The documentation doesn't define this variable but providing for completeness.
#
#
class ckan::ext::scheming (
  Optional[String]  $source           = undef,
  String            $revision         = 'master',
  Array[String]     $plugins          = ['scheming_datasets'],
  String            $data_schemas     = 'ckanext.scheming:ckan_dataset.json',
  String            $presets          = 'ckanext.scheming:presets.json',
  Boolean           $dataset_fallback = false,
) {
  ckan::ext { 'scheming':
    source   => $source,
    revision => $revision,
    plugin   => $plugins,
  }

  ckan::conf::setting { 'scheming.dataset_schemas':
    value   => $data_schemas,
    require => Class['ckan::conf::production'],
  }

  ckan::conf::setting { 'scheming.presets':
    value   => $presets,
    require => Class['ckan::conf::production'],
  }

  ckan::conf::setting { 'scheming.dataset_fallback':
    value   => $dataset_fallback,
    require => Class['ckan::conf::production'],
  }
}
