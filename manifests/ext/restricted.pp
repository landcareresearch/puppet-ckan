# @summary Installs the restricted extension.
#
# @see https://github.com/EnviDat/ckanext-restricted
#
class ckan::ext::restricted () {
  ckan::ext { 'restricted':
    source   => 'http://github.com/EnviDat/ckanext-restricted',
    plugin   => ['restricted'],
    revision => 'master',
  }

  # Recommended requirements
  include ckan::ext::repeating
  include ckan::ext::composite

  file { "${ckan::ckan_default}/schemas":
    ensure => directory,
  }

  file { "${ckan::ckan_default}/schemas/restricted_scheming.json":
    ensure  => file,
    source  => 'puppet:///modules/ckan/ext/restricted_scheming.json',
    require => File["${ckan::ckan_default}/schemas"],
  }

  class { 'ckan::ext::scheming':
    data_schemas => "file:///${ckan::ckan_default}/schemas/restricted_scheming.json",
    presets      => 'ckanext.scheming:presets.json ckanext.repeating:presets.json ckanext.composite:presets.json',
    require      => File["${ckan::ckan_default}/schemas/restricted_scheming.json"],
  }
}
