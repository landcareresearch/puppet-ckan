# @summary Installs the "hierarchy" extension from data.gov.uk.
#
# This extension allows organizations to have parents, and displays
# them in a tree.
#
# @note Untested so probably needs some development effort.
#
# @see https://github.com/datagovuk/ckanext-hierarchy
#
class ckan::ext::hierarchy {
  ckan::ext { 'hierarchy':
    source   => 'http://github.com/datagovuk/ckanext-hierarchy',
    revision => 'master',
    plugin   => ['hierarchy_form', 'hierarchy_display'],
  }
}
