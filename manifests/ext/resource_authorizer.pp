# @summary Installs the resource authorizer extension.
#
# @see https://github.com/etri-odp/ckanext-resourceauthorizer
#
# @param ckan_conf
#   The path to the ckan configuration file.
#
# @param paster
#   The paster command to use.
#
class ckan::ext::resource_authorizer (
  String $ckan_conf = $ckan::params::ckan_conf,
  String $paster    = $ckan::params::paster,
) {
  ckan::ext { 'resourceauthorizer':
    plugin   => ['resourceauthorizer'],
    source   => 'http://github.com/etri-odp/ckanext-resourceauthorizer',
    revision => 'master',
  }

  check_run::task { 'init_resource_authorizer_db':
    exec_command => "${ckan::paster} --plugin=ckanext-resourceauthorizer resourceauthorizer initdb --config=${ckan_conf}",
    require      => Ckan::Ext['resourceauthorizer'],
  }
  check_run::task { 'resource_authorizer_rebuild_search_index':
    exec_command => "${ckan::paster} --plugin=ckan search-index rebuild --config=${ckan_conf}",
    require      => Check_run::Task['init_resource_authorizer_db'],
  }
}
