# @summary Installs the harvest extension.
#
# @see https://github.com/ckan/ckanext-harvest
#
# @param [String] revision
#   The revision to use (a git label or git branch).
#
# @param [String] ckan_conf
#
# @param [String] paster
#
# @param [String] cron_job
#   Sets the ensure state of the cron job.
#   Options: the standard options for the ensure parameter.
#
# @param [String] cron_interval
#   The minute that cron will run (so will run every hour at the
#   specified minute).
#
# @param [String] cron_warning
#   Check if there is no previous long running harvester instance,
#   if there is and has been running longer then $WARN seconds complain about
#   it as it might be stuck.
#
class ckan::ext::harvest (
  String $revision      = 'v1.1.1',
  String $ckan_conf     = $ckan::params::ckan_conf,
  String $paster        = $ckan::params::paster,
  String $cron_job      = 'present',
  String $cron_interval = '5',
  String $cron_warning  = '21600',
) {
  validate_integer($cron_interval)
  validate_integer($cron_warning)

  ckan::ext { 'harvest':
    revision => $revision,
    plugin   => ['harvest', 'ckan_harvester'],
  }

  if $ckan::ckan_version == '2.8' or $ckan::ckan_version == '2.7' or $ckan::ckan_version == '2.6' {
    check_run::task { 'install-ckan-harvest-init':
      exec_command => "${ckan::paster} --plugin=ckanext-harvest harvester initdb --config=${ckan_conf}",
      require      => Ckan::Ext['harvest'],
    }
  } else {
    # >= version 2.9
    check_run::task { 'install-ckan-harvest-init':
      exec_command => "${ckan::paster} --plugin=ckanext-harvest harvester init --config=${ckan_conf}",
      require      => Ckan::Ext['harvest'],
    }
  }

  # Harvester perodically needs to go through running jobs to
  # mark them as finished
  file { '/usr/local/bin/harvest_run.bash':
    ensure  => $cron_job,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => template("${module_name}/ext/harvest_run.bash.erb"),
  }
  cron { 'harvest_run':
    ensure  => $cron_job,
    minute  => $cron_interval,
    user    => 'root',
    command => '/usr/local/bin/harvest_run.bash',
    require => File['/usr/local/bin/harvest_run.bash'],
  }

  ckan::conf::setting { 'ckan.harvest.mq.type':
    value => 'redis',
  }

  #Two unlisted requirements
  ckan::pip_package { 'mock':
    require => Check_run::Task['install-ckan-harvest-init'],
  }

  #And it needs some background jobs running, because $REASONS
  #Using supervisor to do this isn't terrible
  include ckan::supervisor

  ckan::supervisor::program { 'harvester_gather_consumer':
    command => "/usr/lib/ckan/default/bin/paster --plugin=ckanext-harvest harvester gather_consumer --config=${ckan::ckan_conf}",
    require => Ckan::Pip_package['mock'],
  }
  ckan::supervisor::program { 'harvester_fetch_consumer':
    command => "/usr/lib/ckan/default/bin/paster --plugin=ckanext-harvest harvester fetch_consumer --config=${ckan::ckan_conf}",
    require => Ckan::Supervisor::Program['harvester_gather_consumer'],
  }
}
