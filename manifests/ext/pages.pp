# @summary Installs the pages extension.
#
# @see https://github.com/ckan/ckanext-pages
#
class ckan::ext::pages {
  ckan::ext { 'pages':
    plugin   => ['pages'],
    revision => 'master',
  }
}
