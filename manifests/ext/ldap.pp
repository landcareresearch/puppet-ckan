# @summary Installs the "ldap" extension.
#
# @param [String] uri
#   The uri to the ldap server to connect.
#   @example 'ldap://localhost:389'
#
# @param [String] base_dn
#   The ldap base dn to use for user authentication.
#   @example 'ou=users,dc=landcareresearch,dc=co,dc=nz'
#
# @param [String] search_filter
#   The filter for searching through identities in ldap.
#
# @param [String] username
#   The user name to use as a lookup.
#
# @param [String] email
#   The field that contains the user's email address.
#
# @param [String] fullname
#   The field that contains the user's full name.
#
# @param [String] organization_role
#   The role of the user when logged in through ldap.
#
# @param [Optional[String]] organization_id
#   If this is set, users that log in using LDAP will automatically get added
#   to the given organization.
#
#   To create the organisation specified in ckanext.ldap.organization.id use
#   the paste command:
#
#   ```
#    paster --plugin=ckanext-ldap ldap setup-org -c
#   /etc/ckan/default/development.ini
#   ```
#
# @param revision
#   The revision of the ldap repository.
#
# @param source
#   The source version of the ldap respository.
#
# @param use_fallback
#   true if the system should use the ckan database for user authentication if LDAP auth fails.
#
class ckan::ext::ldap (
  String                 $uri,
  String                 $base_dn,
  Optional[String]       $organization_id   = undef,
  String                 $search_filter     = 'uid={login}',
  String                 $username          = 'uid',
  String                 $email             = 'mail',
  String                 $fullname          = 'cn',
  String                 $organization_role = 'member',
  String                 $revision          = 'master',
  Enum['ofkn','history'] $source            = 'ofkn',
  Boolean                $use_fallback      = false,
) {
  # required packages for ldap integration
  $required_packages = ['libldap2-dev','libsasl2-dev','libssl-dev','python-dev','python-ldap']

  ensure_packages($required_packages)

  if $source == 'ofkn' {
    $source_url = 'http://github.com/okfn/ckanext-ldap'
    $run_setup  = false
    $branch     = 'master'
  } else {
    $source_url = 'http://github.com/NaturalHistoryMuseum/ckanext-ldap'
    $run_setup  = true
    $branch     = 'main'
  }

  ckan::ext { 'ldap':
    source           => $source_url,
    revision         => $revision,
    branch           => $branch,
    plugin           => ['ldap'],
    pip_requirements => 'requirements.txt',
    run_setup        => $run_setup,
    require          => Package[$required_packages],
  }

  # set configuration
  # LDAP Extension
  ckan::conf::setting { 'ckanext.ldap.uri':
    value   => $uri,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.base_dn':
    value   => $base_dn,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.search.filter':
    value   => $search_filter,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.username':
    value   => $username,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.email':
    value   => $email,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.fullname':
    value   => $fullname,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.organization.id':
    value   => $organization_id,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.organization.role':
    value   => $organization_role,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckanext.ldap.ckan_fallback':
    value   => $use_fallback,
    require => Class['ckan::conf::production'],
  }

  # check if ldap is using a default organization
  if $organization_id {
    file { '/usr/local/bin/ckan_ldap_setup_org.bash':
      ensure  => file,
      source  => 'puppet:///modules/ckan/ext/ckan_ldap_setup_org.bash',
      mode    => '0755',
      require => Ckan::Ext['ldap'],
    }
    file { "${ckan::ckan_etc}/ldap_setup_org.txt":
      ensure  => file,
      content => "${organization_id}\n",
      require => File['/usr/local/bin/ckan_ldap_setup_org.bash'],
    }
    # run command if organization_id has been changed :(
    exec { 'ckan_ldap_setup_org':
      command     => '/usr/local/bin/ckan_ldap_setup_org.bash',
      cwd         => '/usr/local/bin',
      refreshonly => true,
      subscribe   => File["${ckan::ckan_etc}/ldap_setup_org.txt"],
    }
  }
}
