# @summary Installs the "googleanalytics" extension.
#
# Sends tracking data to Google Analytics and retrieves statistics from
# Google Analytics and inserts them into CKAN pages.
#
# You should enable the "googleanalytics" plugin to use this extension.
#
# @see https://github.com/ckan/ckanext-googleanalytics
#
# @param [String] id
#   The Google Analytics tracking ID (usually of the form UA-XXXXXX-X).
#
# @param [String] account
#   The account name 
#   @example example.com
#   @see https://www.google.com/analytics).
#
# @param [String] username
#   Google Analytics username.
#
# @param [String] password
#   Google Analytics password.
#
# @param [Boolean] track_events
#   Adds Google Analystics Event tracking.
#
# @param [String] revision
#   The revision (or version) of the source to checkout with git.
#
class ckan::ext::googleanalytics (
  String  $id,
  String  $account,
  String  $username,
  String  $password,
  Boolean $track_events = false,
  String  $revision     = 'master',
) {
  # need to install 
  # apt install  libbz2-dev
  # liblzma-dev
  # python3 install rust
  # python3 install setuptools_rust

  ckan::ext { 'googleanalytics':
    revision => $revision,
    plugin   => ['googleanalytics'],
  }

  # Google Analytics extension
  ckan::conf::setting { 'googleanalytics.id':
    value   => $id,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'googleanalytics.account':
    value   => $account,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'googleanalytics.username':
    value   => $username,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'googleanalytics.password':
    value   => $password,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'googleanalytics.track_events':
    value   => $track_events,
    require => Class['ckan::conf::production'],
  }

  # setup cron job to push events
  if $track_events {
    # run the cron every hour
    cron { 'analystics_push_events':
      command => "${ckan::paster} --plugin=ckan tracking update\
 -c ${ckan::ckan_conf} && ${ckan::paster} --plugin=ckan\
 search-index rebuild -r -c ${ckan::ckan_conf}",
      hour    => '*/1',
    }
  }
}
