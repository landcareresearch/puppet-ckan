# @summary Installs the report extension.
#
# @see https://github.com/datagovuk/ckanext-report
#
# @param [String] ckan_conf
#
# @param [String] paster
#
class ckan::ext::report (
  String $ckan_conf = $ckan::params::ckan_conf,
  String $paster    = $ckan::params::paster,
) {
  ckan::ext { 'report':
    plugin   => ['report'],
    source   => 'http://github.com/datagovuk/ckanext-report',
    revision => 'master',
  }

  check_run::task { 'install-ckan-report-init':
    exec_command => "${ckan::paster} --plugin=ckanext-report report initdb --config=${ckan_conf}",
    require      => Ckan::Ext['report'],
  }
}
