# @summary Installs the "spatial" extension.
#
# Allows for the association of datasets with geographic locations, and
# for searches across datasets to be restricted to a particular geographical
# area. Additionally, it provides some support for previewing
# geographical datasets.
#
# @see http://docs.ckan.org/projects/ckanext-spatial/en/latest/
#
# @param [String] template
#   The name the template that spatial will modify.
#   Note, this will overwrite the following files:
#
#   * templates/package/search.html
#   * templates/package/read_base.html
#
# @param [String] map_provider
#   Which map provider to use. Options:
#
#   * 'default' - removes older configuration which may not work with newer versions of ckanext-spatial.
#   * 'openstreetmap' - the older settings which may not work due to cross-origin error.
#   * 'mapquest' - depricated, do not use
#   * 'custom' - uses a custom type that requires the map_url parameter.
#
# @param map_type
#   Depricated, no longer used.
#
# @param map_url
#   The tileset URL.  This follows the Leaflet URL template format ( {z} for zoom and {x} {y} for tile coordinates)
#
# @param map_attribution
#   The attribution for the map tiles and license.  This expects html formatting for urls like <a href=></a>.
#
# @param [Optional[String]] default_extent
#   Sets the default extent for the map.  Should be a string in the format:
#
#   ```
#   '[[15.62, -139.21], [64.92, -61.87]]'
#   ```
#
#    or GeoJSON
#
#   ```
#   '{ \"type\":
#    \"Polygon\", \"coordinates\": [[[74.89, 29.39],[74.89, 38.45], [60.50,
#    38.45], [60.50, 29.39], [74.89, 29.39]]]}'
#    ```
#
#   If undefined, will not set an extent.
#
# @param non_extra_spatial
#   Set to false if the spatial field is not in the extra dictionary.
#
# @param revision
#   The version of spatial to install.
#
# @param add_spatial_search_widget
#   True if the spatial search should be added and false otherwise.
#
class ckan::ext::spatial (
  String           $template,
  String           $map_provider              = 'default',
  Optional[String] $map_type                  = undef,
  Optional[String] $map_url                   = undef,
  Optional[String] $map_attribution           = undef,
  Optional[String] $default_extent            = undef,
  Boolean          $non_extra_spatial         = false,
  Optional[String] $revision                  = undef,
  Boolean          $add_spatial_search_widget = true,
) {
  $package_root = "${ckan::ckan_ext}/ckanext-${template}/ckanext/${template}/templates/package"
  $search       = "${package_root}/search.html"
  $read_base    = "${package_root}/read_base.html"

  if $default_extent {
    $use_default_extent = true
  } else {
    $use_default_extent = false
  }

  # check for OS version
  if $facts['os']['release']['full'] == '12.04' or $facts['os']['release']['full'] == '14.04' {
    $required_packages = [
      'libxml2-dev',
      'libxslt1-dev',
      'libgeos-c1',
    ]
  } else {
    $required_packages = [
      'libxml2-dev',
      'libxslt1-dev',
      'libgeos-c1v5',
    ]
  }

  ensure_packages($required_packages)

  # check if need to install external libraries for solr
  if $ckan::solr_schema_version == 'spatial-ext' {
    solr::shared_lib { 'jts':
      url     => $ckan::jts_url,
      require => Class['solr'],
    }
    solr::shared_lib { 'mmseg4j':
      url     => 'https://repo1.maven.org/maven2/com/chenlb/mmseg4j/mmseg4j-solr/2.4.0/mmseg4j-solr-2.4.0.jar',
      require => Class['solr'],
    }
    $backend = 'solr-spatial-field'
  } else {
    $backend = 'solr'
  }

  postgresql_psql { 'init_db_spatial':
    db      => 'ckan_default',
    command => 'create extension postgis',
    unless  => 'select * from pg_extension where extname=\'postgis\'',
    require => Postgresql::Server::Database['ckan_default'],
  }

  postgresql_psql { 'init_db_topology':
    db      => 'ckan_default',
    command => 'create extension postgis_topology',
    unless  => 'select * from pg_extension where extname=\'postgis_topology\'',
    require => Postgresql_psql['init_db_spatial'],
  }

  postgresql::server::grant { 'schema_grant_all':
    privilege   => 'ALL PRIVILEGES',
    object_type => 'SCHEMA',
    object_name => 'topology',
    db          => 'ckan_default',
    role        => 'ckan_default',
    require     => Postgresql_psql['init_db_topology'],
  }

  postgresql::server::grant { 'grant_all_tables':
    privilege   => 'ALL PRIVILEGES',
    object_type => 'ALL TABLES IN SCHEMA',
    object_name => 'topology',
    db          => 'ckan_default',
    role        => 'ckan_default',
    require     => Postgresql::Server::Grant['schema_grant_all'],
  }

  ckan::ext { 'spatial':
    plugin    => ['spatial_metadata', 'spatial_query'],
    run_setup => true,
    revision  => $revision,
    require   => Postgresql::Server::Grant['grant_all_tables'],
  }

  ckan::conf::setting { 'ckanext.spatial.search_backend':
    value   => $backend,
  }

  # ensure the package directory exists
  file { $package_root:
    ensure  => directory,
    #require => Package['python-ckan'],
    require => File['/usr/lib/ckan/default/src'],
  }

  # add spatial search widget
  if $add_spatial_search_widget {
    file { $search:
      ensure  => file,
      content => template('ckan/ext/search.html.erb'),
      require => File[$package_root],
    }
    file { $read_base:
      ensure  => file,
      content => epp('ckan/ext/read_base.html.epp', {
          non_extra_spatial => $non_extra_spatial
      }),
      require => File[$search],
    }
  }

  case $map_provider {
    'default' : {
      ckan::conf::setting { 'ckanext.spatial.common_map.type':
        ensure  => absent,
        value   => '',
        require => Class['ckan::conf::production'],
      }
      ckan::conf::setting { 'ckanext.spatial.common_map.custom.url':
        ensure  => absent,
        value   => '',
        require => Class['ckan::conf::production'],
      }
      ckan::conf::setting { 'ckanext.spatial.common_map.attribution':
        ensure  => absent,
        value   => '',
        require => Class['ckan::conf::production'],
      }
    }
    'openstreetmap': {
      ckan::conf::setting { 'ckanext.spatial.common_map.type':
        value   => 'custom',
        require => Class['ckan::conf::production'],
      }
      ckan::conf::setting { 'ckanext.spatial.common_map.custom.url':
        value   => 'http://a.tile.openstreetmaps.org/{z}/{x}/{y}.png',
        require => Class['ckan::conf::production'],
      }
      ckan::conf::setting { 'ckanext.spatial.common_map.attribution':
        value   => "&copy <a href=\"http://www.openstreetmap.org/copyright\" target=\"_blank\">OpenStreetMap</a> contributors.\
  Tiles provided by <a href=\"https://maptiles.xyz\">MapTiles</a>.",
        require => Class['ckan::conf::production'],
      }
    }
    'mapquest': {
      #Nothing to do, this has been depricated.
    }
    'custom' : {
      ckan::conf::setting { 'ckanext.spatial.common_map.type':
        value   => 'custom',
        require => Class['ckan::conf::production'],
      }
      ckan::conf::setting { 'ckanext.spatial.common_map.custom.url':
        value   => $map_url,
        require => Class['ckan::conf::production'],
      }
      if $map_attribution {
        ckan::conf::setting { 'ckanext.spatial.common_map.attribution':
          value   => $map_attribution,
          require => Class['ckan::conf::production'],
        }
      }
    }
    default: {
      error('Unknown map_provider')
    }
  }
}
