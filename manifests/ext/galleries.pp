# @summary Installs the "galleries" extension.
#
# This extension allows storing and referencing image and video assets.
#
# This extension does not install properly on Ubuntu 12.04.
#
# @see https://github.com/DataShades/ckan-galleries
#
class ckan::ext::galleries {
  $packages = ['libmysqlclient-dev','libjpeg-dev','python-dev']

  ensure_packages($packages)

  # required and will fail without it.
  file { '/var/www/.flickr':
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    require => Package['libmysqlclient-dev'],
  }

  ckan::ext { 'galleries':
    source          => 'http://github.com/DataShades/ckan-galleries',
    extname         => 'galleries',
    revision        => 'master',
    plugin          => ['dfmp'],
    run_setup       => true,
    run_setup_param => 'install',
    require         => File['/var/www/.flickr'],
  }

  # needs to be done via virtual env
  #. /usr/lib/ckan/default/bin/activate
  #exec { 'setup_galleries':
  #  command     => "${ckan::activate_exec} ${ckan::python} setup.py install",
  #  cwd         => '/usr/lib/ckan/default/src/ckanext-galleries',
  #  refreshonly => true,
  #  subscribe   => Ckan::Ext['galleries'],
  #}
}
