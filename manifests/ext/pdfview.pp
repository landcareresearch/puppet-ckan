# @summary Installs the "pdf_view" extension
#
# If you want to enable pdf views of documents outside of
# the ckan instance, you will need to enable resource_proxy plugin.
#
# @see https://github.com/ckan/ckanext-pdfview
#
class ckan::ext::pdfview {
  ckan::ext { 'pdfview':
    source   => 'https://github.com/ckan/ckanext-pdfview',
    revision => 'master',
    plugin   => ['pdf_view'],
    views    => ['pdf_view'],
  }
}
