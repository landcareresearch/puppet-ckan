# @summary Installs the "repeating" extension.
#
# This extension provides a way to store repeating fields in CKAN datasets,
# resources, organizations and groups.
#
# @see https://github.com/open-data/ckanext-repeating
#
class ckan::ext::repeating {
  ckan::ext { 'repeating':
    source   => 'http://github.com/open-data/ckanext-repeating',
    revision => 'master',
    plugin   => ['repeating'],
  }
}
