# @summary Installs the zippreview extension.
#
# @see https://github.com/datagovau/ckanext-zippreview
#
class ckan::ext::zippreview {
  ckan::ext { 'zippreview':
    source           => 'https://github.com/datagovau/ckanext-zippreview',
    plugin           => ['zip_view'],
    views            => ['zip_view'],
    revision         => 'Develop',
    branch           => 'Develop',
    pip_requirements => 'requirements.txt',
  }
}
