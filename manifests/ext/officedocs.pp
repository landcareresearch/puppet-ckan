# @summary Installs the officedocs extension.
#
# For this extension to work, the documents to be previewed must be accessible
# to the wider internet, and will only work if you use a hostname, and not just
# an IP address.
#
# @see http://extensions.ckan.org/extension/officedocs/
#
# @param [String] ckan_conf
#
# @param [String] paster
#
class ckan::ext::officedocs (
  String $ckan_conf = $ckan::params::ckan_conf,
  String $paster    = $ckan::params::paster,
) {
  ckan::ext { 'officedocs':
    source           => 'https://github.com/jqnatividad/ckanext-officedocs',
    plugin           => ['officedocs_view'],
    views            => ['officedocs_view'],
    revision         => 'master',
    pip_requirements => 'requirements.txt',
    run_setup        => true,
    run_setup_param  => 'install',
  }
}
