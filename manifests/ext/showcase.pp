# @summary Installs the showcase extension.
#
# @see https://github.com/ckan/ckanext-showcase
#
class ckan::ext::showcase {
  ckan::ext { 'showcase':
    plugin   => ['showcase'],
    revision => 'master',
  }
}
