# @summary Installs the archiver extension.
#
# @see https://github.com/ckan/ckanext-archiver
#
# @param [String] ckan_conf
#
# @param [String] paster
#
class ckan::ext::archiver (
  String $ckan_conf = $ckan::params::ckan_conf,
  String $paster    = $ckan::params::paster,
) {
  ckan::ext { 'archiver':
    plugin           => ['archiver'],
    revision         => 'master',
    pip_requirements => 'requirements.txt',
  }

  check_run::task { 'install-ckan-archiver-init':
    exec_command => "${ckan::paster} --plugin=ckanext-archiver archiver\
 init --config=${ckan_conf}",
    require      => Ckan::Ext['archiver'],
  }

  $main_section = 'app:celery'
  ckan::conf::setting { 'BROKER_BACKEND':
    value   => 'redis',
    section => $main_section,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'BROKER_HOST':
    value   => 'redis://localhost/1',
    section => $main_section,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'CELERY_RESULT_BACKEND':
    value   => '127.0.0.1',
    section => $main_section,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'REDIS_HOST':
    value   => '127.0.0.1',
    section => $main_section,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'REDIS_PORT':
    value   => '6379',
    section => $main_section,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'REDIS_DB':
    value   => '0',
    section => $main_section,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'REDIS_CONNECT_RETRY':
    value   => 'True',
    section => $main_section,
    require => Class['ckan::conf::production'],
  }

  # Requires redis pip, but the redis pip is not listed in
  # requirements of the extension.
  ckan::pip_package { 'redis':
    version => '2.10.1',
    require => Check_run::Task['install-ckan-archiver-init'],
  }
}
