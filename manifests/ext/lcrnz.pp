# @summary Installs the Landcare Research extension.
#
# **Requirements:**
#
#  Installs the following extensions.
#
# * ckan::ext::repeating
# * ckan::ext::ldap
#
# @param uri
#   The uri to the ldap server to connect.
#   @example 'ldap://localhost:389'
#
# @param base_dn
#   The ldap base dn to use for user authentication.
#   @example 'ou=users,dc=landcareresearch,dc=co,dc=nz'
#
# @param source
#   The source of the git repository for this extension.
#
# @param revision
#   The revision (or version) of the source to checkout with git.
#
# @param install_ldap
#   True if the ldap module should be installed and false otehrwise.
#
# @param install_repeating
#   True if the repeating module should be installed and false otehrwise.
#
# @param ldap_revision
#   The revision (or version) of the ldap extension source to checkout with git.
#
# @param ldap_source
#   The source of the ldap extension source to checkout with git.
#
# @param organization_id
#   If this is set, users that log in using LDAP will automatically get added
#   to the given organization.
#
# The id can be found at the following url:
# http://<CKAN URL/api/action/organization_show?id=<ORG NAME>
#
class ckan::ext::lcrnz (
  String           $source                    = 'http://github.com/okfn/ckanext-lcrnz',
  String           $revision                  = 'master',
  Boolean          $install_ldap              = true,
  Boolean          $install_repeating         = true,
  String           $ldap_revision             = 'master',
  String           $ldap_source               = 'ofkn',
  Optional[String] $uri                       = undef,
  Optional[String] $base_dn                   = undef,
  Optional[String] $organization_id           = undef,
) {
  if $install_repeating {
    contain ckan::ext::repeating
  }
  if $install_ldap {
    class { 'ckan::ext::ldap':
      uri             => $uri,
      base_dn         => $base_dn,
      organization_id => $organization_id,
      source          => $ldap_source,
      revision        => $ldap_revision,
    }
    contain ckan::ext::ldap
  }
  ckan::ext { 'lcrnz':
    source   => $source,
    revision => $revision,
    plugin   => ['lcrnz'],
  }
## Custom i18n folder (for replacing "organizations" with "collections")
## Update the path if necessary
#ckan.locale_default = en_NZ
#ckan.i18n_directory = /usr/lib/ckan/default/src/ckanext-lcrnz
}
