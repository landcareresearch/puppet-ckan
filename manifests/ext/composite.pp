# @summary Installs the composite extension.
#
# @see https://github.com/EnviDat/ckanext-composite
#
class ckan::ext::composite () {
  ckan::ext { 'composite':
    source   => 'http://github.com/EnviDat/ckanext-composite',
    plugin   => ['composite'],
    #revision => 'master',
    revision => 'ckan-2.8',
  }
}
