# @summary This CKAN extension allows a user to create private datasets.
#
# Only certain users will be able to see. When a dataset is being created,
# it's possible to specify the list of users that can see this dataset.
#
# @see https://github.com/conwetlab/ckanext-privatedatasets
#
# @param [Boolean] show_acquire_url_on_create
#   To show the Acquire URL when the user is creating a dataset.
#
# @param [Boolean] show_acquire_url_on_edit
#   To show the Acquire URL when the user is editing a dataset
#
class ckan::ext::private_datasets (
  Boolean $show_acquire_url_on_create = false,
  Boolean $show_acquire_url_on_edit   = false,
) {
  ckan::ext { 'privatedatasets':
    source   => 'http://github.com/conwetlab/ckanext-privatedatasets',
    revision => 'master',
    plugin   => ['privatedatasets'],
  }
  $parser = 'ckanext.privatedatasets.parsers.fiware:FiWareNotificationParser'

  # Private Datasets Extension
  ckan::conf::setting { 'ckan.privatedatasets.parser':
    value   => $parser,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckan.privatedatasets.show_acquire_url_on_create':
    value   => $show_acquire_url_on_create,
    require => Class['ckan::conf::production'],
  }
  ckan::conf::setting { 'ckan.privatedatasets.show_acquire_url_on_edit':
    value   => $show_acquire_url_on_edit,
    require => Class['ckan::conf::production'],
  }
}
