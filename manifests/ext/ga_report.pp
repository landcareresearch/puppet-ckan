# @summary Installs the google analytics report extension.
# Requires the googleanalytics extension.
#
# @see https://github.com/datagovau/ckanext-ga-report
#
# @param revision
#   The version of dcat to install from github.
#   Can be a git branch or git label.
#
# @param report_period
#   The frequency of building period reports.
#
# @param bounce_url
#   Specifies a particular path to record the bounce rate for. Typically it is / (the home page).
#
class ckan::ext::ga_report (
  String $revision      = 'dga-master',
  String $report_period = 'monthly',
  String $bounce_url    = '/',
) {
  ckan::ext { 'ga_report':
    revision => $revision,
    source   => 'http://github.com/datagovau/ckanext-ga-report',
    plugin   => ['ga-report'],
  }

  ckan::conf::setting { 'ga-report.period':
    value   => $report_period,
  }
  ckan::conf::setting { 'ga-report.bounce_url':
    value   => $bounce_url,
  }
}
