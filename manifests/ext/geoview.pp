# @summary Installs the geoview extension.
#
# **Requirements:**
#
#  In order for this module to update the default_views parameter,
#  it MUST be declared before the ckan module in the manifests.
#  This will allow all of the variables to be evaulated.
#
#  Also, ensure that resource_proxy plugin has been enabled.
#
# @see https://github.com/ckan/ckanext-geoview
#
# @param [Boolean] openlayers_viewer_enable
#
# @param [String] openlayers_formats
#   A string seperated by spaces.
#   Available formats
#   - Web Map Service (WMS)      wms
#   - Web Feature Service (WFS)  wfs
#   - GeoJSON                    geojson
#   - GML                        gml
#   - KML                        kml
#   - ArcGIS REST API            arcgis_rest
#   - Google Fusion Tables       gft (requires google_api_key)
#
# @param [Boolean] openlayers_hide_overlays
#   Overlays won't be visible by default (only the base layer)
#
# @param [Boolean] openlayers_hoveron
#   The feature data popup will be displayed when hovering on.
#
# @param [Optional[String]] openlayers_google_api_key
#   The google api key required to render google fusion table resources.
#   @see https://developers.google.com/fusiontables/docs/v1/using#APIKey
#
# @param [Boolean] leaflet_geojson_viewer_enable
#
# @param [Boolean] leaflet_wmts_viewer_enable
#
class ckan::ext::geoview (
  Boolean          $openlayers_viewer_enable      = true,
  String           $openlayers_formats            = 'wms geojson',
  Boolean          $openlayers_hide_overlays      = false,
  Boolean          $openlayers_hoveron            = true,
  Optional[String] $openlayers_google_api_key     = undef,
  Boolean          $leaflet_geojson_viewer_enable = true,
  Boolean          $leaflet_wmts_viewer_enable    = true,
) {
  ## == openlayers viewer == ##
  if $openlayers_viewer_enable {
    # set the view
    $openlayers_view   = ['geo_view']
    $openlayers_plugin = ['geo_view']

    # add additional configuration to main config file
    ckan::conf::setting { 'ckanext.geoview.ol_viewer.formats':
      value   => $openlayers_formats,
      require => Ckan::Ext['geoview'],
    }

    ckan::conf::setting { 'ckanext.geoview.gapi_key':
      value   => $openlayers_google_api_key,
      require => Ckan::Ext['geoview'],
    }

    ckan::conf::setting { 'ckanext.geoview.ol_viewer.hide_overlays':
      value   => $openlayers_hide_overlays,
      require => Ckan::Ext['geoview'],
    }
    ckan::conf::setting { 'ckanext.geoview.ol_viewer.default_feature_hoveron':
      value   => $openlayers_hoveron,
      require => Ckan::Ext['geoview'],
    }
  } else {
    $openlayers_view   = []
    $openlayers_plugin = []
  }

  ## == geojson == ##
  if $leaflet_geojson_viewer_enable {
    $geojson_view   = ['geojson_view']
    $geojson_plugin = ['geojson_view']
  } else {
    $geojson_view   = []
    $geojson_plugin = []
  }

  ## == wmts == ##
  if $leaflet_wmts_viewer_enable {
    $wmts_view   = ['wmts_view']
    $wmts_plugin = ['wmts_view']
  } else {
    $wmts_view   = []
    $wmts_plugin = []
  }

  # collect variables
  $default_views   = $openlayers_view   + $geojson_view   + $wmts_view
  $plugins         = $openlayers_plugin + $geojson_plugin + $wmts_plugin

  # TODO, default_views should be an ARRAY NOT A STRING!!!!!!
  ckan::ext { 'geoview':
    plugin   => $plugins,
    views    => $default_views,
    revision => 'master',
  }
}
