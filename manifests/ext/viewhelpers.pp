# @summary Installs the viewhelpers extension.
#
# @see https://github.com/ckan/ckanext-viewhelpers
#
#
class ckan::ext::viewhelpers {
  ckan::ext { 'viewhelpers':
    plugin   => ['viewhelpers'],
    revision => 'master',
  }
}
