# @summary Installs the tif imageview extension.
#
# Note, in ckan 2.8, need to manually install Pillow
# pip install Pillow
#
# @param revision
#   The version or branch to used.
#
# @see https://github.com/TIBHannover/ckanext-tif-imageview
#
class ckan::ext::tif_imageview (
  String $revision = 'main',
) {
  ckan::ext { 'tif_imageview':
    source   => 'https://github.com/TIBHannover/ckanext-tif-imageview',
    plugin   => ['tif_imageview'],
    views    => ['tif_imageview'],
    revision => $revision,
    branch   => $revision,
  }
}
