# @summary Installs the dcat extension.
#
# @see https://github.com/ckan/ckanext-dcat
#
# @param [String] revision
#   The version of dcat to install from github.
#   Can be a git branch or git label.
#
# Note, requires the harvest extension.
#
class ckan::ext::dcat (
  String $revision = 'v0.0.7'
) {
  ckan::ext { 'dcat':
    revision         => $revision,
    plugin           => ['dcat', 'dcat_rdf_harvester', 'dcat_json_harvester', 'dcat_json_interface', 'structured_data'],
    pip_requirements => 'requirements.txt',
  }
}
