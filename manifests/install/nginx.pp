# @summary Manages installing and configuring nginx.
#
class ckan::install::nginx {
  $use_nagios                 = $ckan::use_nagios
  $use_google_verification    = $ckan::use_google_verification
  $google_verification_string = $ckan::google_verification_string
  $max_resource_size          = $ckan::nginx_max_resource_size
  $ssl_cert_path              = $ckan::ssl_cert_path
  $ssl_key_path               = $ckan::ssl_key_path
  $use_ssl                    = $ckan::use_ssl
  $disable_bot_pipl           = $ckan::disable_bot_pipl
  $disable_all_bots           = $ckan::disable_all_bots

  if $use_google_verification {
    file { ['/opt/verification','/opt/verification/www']:
      ensure  => directory,
    }
    # verify google
    file { "/opt/verification/www/${google_verification_string}.html":
      ensure  => file,
      content => "google-site-verification: ${google_verification_string}.html",
      require => File['/opt/verification/www'],
      before  => Class['nginx'],
      #before  => File['/etc/nginx/sites-available/ckan'],
      #before  => Class['ckan'],
    }
    if $use_nagios {
      # nagios monitoring (to check if web server is operational)
      file { '/opt/verification/www/nagios.txt':
        ensure  => file,
        content => 'nagios',
        require => File['/opt/verification/www'],
        before  => Class['nginx'],
        #before  => Class['ckan'],
        #before  => File['/etc/nginx/sites-available/ckan'],
      }
    }
  }

  class { 'nginx':
    service_manage        => false,
    proxy_temp_path       => '/tmp/nginx_proxy 1 2',
    proxy_connect_timeout => $ckan::nginx_proxy_connect_timeout,
    proxy_send_timeout    => $ckan::nginx_proxy_send_timeout,
    proxy_read_timeout    => $ckan::nginx_proxy_read_timeout,
    client_max_body_size  => $ckan::nginx_client_max_body_size,
    send_timeout          => $ckan::nginx_send_timeout,
    keepalive_timeout     => $ckan::nginx_keepalive_timeout,
    client_body_temp_path => '/opt/nginx_tmp',
    log_format            => {
      'main' => "\$http_x_forwarded_for - \$remote_user [\$time_local] \"\$request\" '
                      '\$status \$body_bytes_sent \"\$http_referer\" '
                      '\"\$http_user_agent\"",
    },
    http_access_log       => '/var/log/nginx/access.log  main',
  }
  contain nginx

  file { '/etc/nginx/sites-enabled/default':
    ensure  => absent,
    require => Class['nginx'],
  }

#  $robots = ["### CKAN-Inherit from / just for the following to disallow robots
#    if ( \$uri ~ /dataset/rate/|/revision/|/dataset/history/|/api/ ) {
#      add_header X-Robots-Tag \"none, noarchive, nosnippet, noodp, notranslate, noimageindex\";
#    }"]
#
#  $www_root = '/opt/verification/www'
#
#
#  if $use_google_verification {
#    $google_location = ["location = /${google_verification_string}.html {
#        try_files \$uri \$uri/ =404;
#    }"]
#  }else{
#    $google_location = []
#  }
#
#  if $use_nagios {
#    $use_nagios_string = ["location = /nagios.txt {
#        try_files \$uri \$uri/ =404;
#    }"]
#  }else{
#    $use_nagios_string = []
#  }
#
#  if $disable_bot_pipl {
#    $disable_bot_pipl_string = ["# block a bot
#      if (\$http_user_agent ~* (piplbot)) {
#        return 403;
#      }"]
#  }else{
#    $disable_bot_pipl_string = []
#  }
#
#  $location_raw_append = concat($robots,$google_location,$use_nagios_string,$disable_bot_pipl_string)
#
#  if $use_ssl {
#    $location_raw_short = concat( $google_location,$use_nagios_string,$disable_bot_pipl_string)
#      #['return 301 https://$http_host$request_uri;'])
#    nginx::resource::server {'ckan':
#      server_name         => [$ckan::server_name],
#      listen_port         => 80,
#      index_files         => [],
#      location_raw_append => $location_raw_short,
#      ssl_redirect        => true,
#      #raw_prepend         =>[
#      #  'proxy_cache_path /tmp/nginx_cache levels=1:2 keys_zone=cache:30m max_size=250m;',
#      #  'proxy_temp_path  /tmp/nginx_proxy 1 2;',
#      #]
#    }
#    nginx::resource::server {'ckan_ssl':
#      server_name          => [$ckan::server_name],
#      listen_port          => 443,
#      ssl_port             => 443,
#      ssl                  => true,
#      ssl_cert             => $ssl_cert_path,
#      ssl_key              => $ssl_key_path,
#      client_max_body_size => $max_resource_size,
#      proxy                => 'http://127.0.0.1:8080/',
#      proxy_set_header     => ['Host $host',
#                              'X-Real-IP $remote_addr',
#                              'X-Forwarded-For $proxy_add_x_forwarded_for',
#                              'X-Forwarded-Server $host',
#                              'X-Forwarded-Host $host',
#                              'X-Forwarded-Proto $scheme',
#                              'X-Forwarded-Ssl $https'],
#      proxy_cache          => 'cache',
#      proxy_cache_bypass   => ['$cookie_auth_tkt'],
#      proxy_cache_valid    => '30m',
#      proxy_cache_key      => '$host$scheme$proxy_host$request_uri',
#      location_cfg_append  => {proxy_no_cache => '$cookie_auth_tkt'},
#      location_raw_append  => $location_raw_append,
#      #raw_prepend          =>[
#      #  'proxy_cache_path /tmp/nginx_cache levels=1:2 keys_zone=cache:30m max_size=250m;',
#      #  'proxy_temp_path  /tmp/nginx_proxy 1 2;',
#      #]
#    }
#
#  }else {
#    nginx::resource::server {'ckan':
#      server_name          => [$ckan::server_name],
#      listen_port          => 80,
#      client_max_body_size => $max_resource_size,
#      proxy                => 'http://127.0.0.1:8080/',
#      proxy_set_header     => ['Host $host',
#                              'X-Real-IP $remote_addr',
#                              'X-Forwarded-For $proxy_add_x_forwarded_for',
#                              'X-Forwarded-Server $host',
#                              'X-Forwarded-Host $host',
#                              'X-Forwarded-Proto $scheme',
#                              'X-Forwarded-Ssl $https'],
#      proxy_cache          => 'cache',
#      proxy_cache_bypass   => ['$cookie_auth_tkt'],
#      proxy_cache_valid    => '30m',
#      proxy_cache_key      => '$host$scheme$proxy_host$request_uri',
#      location_cfg_append  => {proxy_no_cache => '$cookie_auth_tkt'},
#      location_raw_append  => $location_raw_append,
#      #raw_prepend          =>[
#      #  'proxy_cache_path /tmp/nginx_cache levels=1:2 keys_zone=cache:30m max_size=250m;',
#      #  'proxy_temp_path  /tmp/nginx_proxy 1 2;',
#      #]
#    }
#  }

  # configure xrobots & ssl
  if $use_ssl {
    $nginx_config_template = 'ckan/ckan.sites-available.ssl.epp'
  } else {
    $nginx_config_template = 'ckan/ckan.sites-available.nossl.epp'
  }
  file { '/etc/nginx/sites-available/ckan':
    content => epp($nginx_config_template, {
        use_google_verification    => $use_google_verification,
        use_nagios                 => $use_nagios,
        google_verification_string => $google_verification_string,
        max_resource_size          => $max_resource_size,
        ssl_cert_path              => $ssl_cert_path,
        ssl_key_path               => $ssl_key_path,
        disable_bot_pipl           => $disable_bot_pipl,
        disable_all_bots           => $disable_all_bots,
    }),
    require => Class['nginx'],
  }
  file { '/etc/nginx/sites-enabled/ckan':
    ensure  => link,
    target  => '/etc/nginx/sites-available/ckan',
    require => File['/etc/nginx/sites-available/ckan'],
  }
}
