# @summary install the datapusher
#
class ckan::install::datapusher {
  $datapusher_dir = '/usr/lib/ckan/datapusher'
  $datapusher_src = "${datapusher_dir}/src"
  #$python_version = $ckan::python_version

  case $ckan::datapusher_version {
    '0.0.17','0.0.18' :  {
      $datapusher_python_version = 3
      $python_version = 2
    }
    default : {
      $datapusher_python_version = 2
      $python_version = 2
    }
  }

  file { '/usr/local/bin/ckan_setup_datastore.bash':
    ensure  => file,
    mode    => '0755',
    content => epp('ckan/scripts/ckan_setup_datastore.bash.epp', { python_version => $python_version }),
    require => File['/usr/lib/ckan','/usr/lib/ckan/default'],
  }

  file { '/usr/local/bin/ckan_install_datapusher.bash':
    ensure  => file,
    mode    => '0755',
    content => epp('ckan/scripts/ckan_install_datapusher.bash.epp', { python_version => $datapusher_python_version }),
    require => File['/usr/local/bin/ckan_setup_datastore.bash'],
  }

  file { [$datapusher_dir,$datapusher_src]:
    ensure  => directory,
    require => File['/usr/local/bin/ckan_install_datapusher.bash'],
  }
  vcsrepo { $datapusher_src:
    ensure   => present,
    provider => 'git',
    source   => 'https://github.com/ckan/datapusher.git',
    revision => $ckan::datapusher_version,
  }
  exec { 'install_datapusher':
    command => '/usr/local/bin/ckan_install_datapusher.bash',
    creates => "${datapusher_src}/datapusher.egg-info/PKG-INFO",
    timeout => 0,
    require => Vcsrepo[$datapusher_src],
  }
}
