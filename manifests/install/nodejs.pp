# @summary Install Node and NPM (which comes with Node)
#
class ckan::install::nodejs {
  # Install Node and NPM (which comes with Node)

  # problems with Ubuntu 12.04
  if $facts['os']['name'] == 'Ubuntu' and $facts['os']['release']['full'] == '12.04' {
    # less requires a compile of the css before changes take effect.
    exec { 'Install Less and Nodewatch':
      command => "/usr/bin/curl -sL https://deb.nodesource.com/setup | bash &&\
/usr/bin/apt-get update &&\
/usr/bin/apt-get install nodejs &&\
/usr/bin/npm install less nodewatch",
      cwd     => '/usr/lib/ckan/default/src/ckan',
      creates => '/usr/lib/ckan/default/src/ckan/bin/less',
    }
  } else {
    contain nodejs
  }
}
