# @summary install the datapusher
#
class ckan::install::datapusher_plus {
  $datapusher_dir = '/usr/lib/ckan/datapusher-plus'
  $datapusher_src = "${datapusher_dir}/src"
  $settings_dir   = '/etc/ckan/datapusher'
  $store_pass     = $ckan::datapusher_plus_password
  $jobs_pass      = $ckan::datapusher_plus_password

  ensure_packages(['python3.7','python3.7-venv','python3.7-dev'])

  file { '/usr/local/bin/ckan_setup_datastore.bash':
    ensure  => file,
    mode    => '0755',
    content => epp('ckan/scripts/ckan_setup_datastore.bash.epp', { python_version => $ckan::python_version }),
    require => [File['/usr/lib/ckan','/usr/lib/ckan/default'],Package['python3.7-venv']],
  }

  file { '/usr/local/bin/ckan_install_datapusher_plus.bash':
    ensure  => file,
    mode    => '0755',
    content => epp('ckan/scripts/ckan_install_datapusher_plus.bash.epp', { python_version => $ckan::python_version }),
    require => File['/usr/local/bin/ckan_setup_datastore.bash'],
  }

  file { [$datapusher_dir,$datapusher_src]:
    ensure  => directory,
    require => File['/usr/local/bin/ckan_install_datapusher_plus.bash'],
  }

  vcsrepo { $datapusher_src:
    ensure   => present,
    provider => 'git',
    source   => 'https://github.com/dathere/datapusher-plus.git',
    revision => $ckan::datapusher_version,
  }

  exec { 'install_datapusher':
    command => '/usr/local/bin/ckan_install_datapusher_plus.bash',
    creates => "${datapusher_src}/datapusher.egg-info/PKG-INFO",
    timeout => 0,
    require => Vcsrepo[$datapusher_src],
  }

  file { '/opt/qsv':
    ensure  => directory,
    require => Exec['install_datapusher'],
  }

  $qsv_file = 'qsv-0.61.1-x86_64-unknown-linux-gnu.zip'
  archive { 'qsv':
    extract_path => '/usr/local/bin',
    source       => "https://github.com/jqnatividad/qsv/releases/download/0.61.1/${qsv_file}",
    extract      => true,
    path         => "/opt/qsv/${qsv_file}",
    require      => File['/opt/qsv'],
  }
}
