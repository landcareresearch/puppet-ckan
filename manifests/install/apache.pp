# @summary manages the apache configuration
#
class ckan::install::apache {
  if $ckan::install_datapusher_plus {
    $wsgi_source = 'puppet:///modules/ckan/datapusher_plus/datapusher.wsgi'

    $settings_dir   = '/etc/ckan/datapusher'
    file { $settings_dir:
      ensure  => directory,
    }

    file { "${settings_dir}/uwsgi.ini":
      ensure  => file,
      source  => 'puppet:///modules/ckan/datapusher_plus/uwsgi.ini',
      require => File[$settings_dir],
    }

    file { "${settings_dir}/settings.py":
      ensure  => file,
      content => epp('ckan/datapusher_plus/settings.py.epp', {
          store_pass => $ckan::datapusher_plus_password,
          jobs_pass  => $ckan::datapusher_plus_password,
      }),
      require => File[$settings_dir],
    }
  } else {
    case $ckan::datapusher_version {
      '0.0.17' :  {
        $wsgi_source = 'puppet:///modules/ckan/datapusher/0.0.17/datapusher.wsgi'
      }
      '0.0.18' :  {
        $wsgi_source = 'puppet:///modules/ckan/datapusher/0.0.18/datapusher.wsgi'
      }
      default : {
        $wsgi_source = 'puppet:///modules/ckan/datapusher/datapusher.wsgi'
      }
    }
  }

  file { '/etc/ckan/datapusher.wsgi':
    ensure => file,
    source => $wsgi_source,
  }

  case $ckan::datapusher_version {
    '0.0.17' :  {
      $datapusher_settings_file = 'puppet:///modules/ckan/datapusher/0.0.17/settings.py'
    }
    '0.0.18' :  {
      $datapusher_settings_file = 'puppet:///modules/ckan/datapusher/0.0.18/settings.py'
    }
    default : {
      $datapusher_settings_file = 'puppet:///modules/ckan/datapusher/settings.py'
    }
  }

  file { '/etc/ckan/datapusher_settings.py':
    ensure  => file,
    source  => $datapusher_settings_file,
    require => File['/etc/ckan/datapusher.wsgi'],
  }

  file { '/etc/ckan/default/apache.wsgi':
    ensure  => file,
    source  => 'puppet:///modules/ckan/apache.wsgi',
    require => File['/etc/ckan/datapusher_settings.py'],
  }

  class { 'apache':
    default_vhost   => false,
    service_manage  => false,
    purge_configs   => true,
    purge_vhost_dir => true,
    timeout         => $ckan::apache_timeout,
  }
  contain apache
  class { 'apache::mod::wsgi':
    wsgi_socket_prefix  => '/var/run/wsgi',
  }
  contain apache::mod::wsgi
  contain apache::mod::rpaf

  apache::vhost { 'default_ckan':
    port                        => '8080',
    ip                          => '0.0.0.0',
    servername                  => $ckan::server_name,
    serveraliases               => ["www.${ckan::server_name}"],
    error_log_file              => '/var/log/apache2/ckan_default.error.log',
    docroot                     => '/var/www',
    default_vhost               => true,
    directories                 => [{
        path     => '/',
        provider => 'directories',
        require  => 'all granted',
    }],
    wsgi_daemon_process         => 'ckan_default',
    wsgi_daemon_process_options => {
      processes    => '2',
      threads      => '15',
      display-name => 'ckan_default',
    },
    wsgi_process_group          => 'ckan_default',
    wsgi_script_aliases         => { '/' => '/etc/ckan/default/apache.wsgi' },
    wsgi_pass_authorization     => 'on',
    custom_fragment             => "CustomLog /var/log/apache2/ckan_default.custom.log combined
    <IfModule mod_rpaf.c>
        RPAFenable On
        RPAFsethostname On
        RPAFproxy_ips 127.0.0.1
    </IfModule>",
  }

  apache::vhost { 'datapusher':
    port                        => '8800',
    ip                          => '0.0.0.0',
    #servername                  => 'ckan',
    servername                  => $ckan::server_name,
    error_log_file              => '/var/log/apache2/datapusher.error.log',
    docroot                     => '/var/www',
    directories                 => [{
        path     => '/',
        provider => 'directories',
        require  => 'all granted',
    }],
    wsgi_daemon_process         => 'datapusher',
    wsgi_daemon_process_options => {
      processes    => '1',
      threads      => '15',
      display-name => 'demo',
    },
    wsgi_process_group          => 'datapusher',
    wsgi_script_aliases         => { '/' => '/etc/ckan/datapusher.wsgi' },
    wsgi_pass_authorization     => 'on',
    custom_fragment             => 'CustomLog /var/log/apache2/datapusher.custom.log combined',
  }
}
