# @summary Install an extra pip module, typically to work around
#          mistakes/limitations in extensions.
#
# @param version
#   Specify the version of the package.
#   Note, its advised to leave as undef.
#
define ckan::pip_package (
  Optional[String] $version = undef,
) {
  $base_command = "/usr/lib/ckan/default/bin/pip install ${name}"

  $command = $version ? {
    undef   => $base_command,
    default => "${base_command}=${version}"
  }

  exec { "ckan pip ${name}":
    command => $command,
    unless  => "/usr/lib/ckan/default/bin/pip list|/bin/grep -q ${name}",
    require => Class['ckan::install'],
  }
}
