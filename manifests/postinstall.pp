# @summary Manages tasks to be performed after the initial install like
# initializing the database
#
# @see http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
class ckan::postinstall {
  # variables
  case $ckan::ckan_version {
    '2.0','2.1','2.2': {
      $set_database_command =
        "/usr/bin/python /usr/lib/ckan/default/src/ckan/ckanext/\
datastore/bin/datastore_setup.py ckan_default datastore_default ckan_default\
 ckan_default datastore_default -p postgres"
    }
    '2.3': {
      $set_database_command = "/usr/bin/sudo ckan datastore set-permissions |
/usr/bin/sudo -u postgres psql --set ON_ERROR_STOP=1"
    }
    default: {
      $set_database_command = '/usr/local/bin/ckan_set_db_permissions.bash'
    }
  }

  check_run::task { 'init_db':
    exec_command => '/usr/local/bin/ckan_init_db.bash',
    before       => Check_run::Task['set_database_perms'],
  }

  check_run::task { 'set_database_perms':
    exec_command => $set_database_command,
  }
}
