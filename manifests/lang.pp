# @summary Manages installing a non default language.
#
# This class expects a zip file that has the contents of the language.
# The contents of the zip file should have the following:
#
#    * LC_MESSAGES/ckan.mo
#    * LC_MESSAGES/ckan.po
#
# @param url
#   The url to the language build.  The build is a zip file with a root
#   directory.
#
# @param language
#   The language that is to be installed.
#
# @param web_user
#   The web user to use if authentication is necessary.
#
# @param web_password
#   The web user's password if authentication is necessary.
#
define ckan::lang (
  String           $url,
  String           $language     = $title,
  Optional[String] $web_user     = undef,
  Optional[String] $web_password = undef,
) {
  include deploy_zip

  deploy_zip::zip { $language:
    deployment_dir => $ckan::ckan_lang_dir,
    url            => $url,
    web_user       => $web_user,
    web_password   => $web_password,
    before         => Class['ckan::service'],
  }

  file { $ckan::ckan_lang_base:
    ensure  => directory,
    recurse => true,
    mode    => '0777',
    require => Deploy_zip::Zip[$language],
  }
}
