# @summary Installs ckan and related software.
#
# @see http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
class ckan::install {
  ## Variables

  # determine ckan
  if $ckan::ckan_version == undef {
    # report error
    fail('ckan_version not set!')
  }

  # use ckan.org package repository if not specified
  if $ckan::ckan_package_url == undef and $ckan::ckan_package_filename == undef {
    $codename              = $facts['os']['distro']['codename']
    $ckan_package_filename = "python-ckan_${ckan::ckan_version}-${codename}_amd64.deb"
    $ckan_package_url      = "http://packaging.ckan.org/${ckan_package_filename}"
  } else {
    $ckan_package_filename = $ckan::ckan_package_filename
    $ckan_package_url      = $ckan::ckan_package_url
  }

  # determine solr
  if $ckan::solr_schema_version == 'default' {
    $solr_schema_path = "${ckan::ckan_src}/ckan/config/solr/schema.xml"
  } elsif $ckan::solr_schema_version == 'spatial' or $ckan::solr_schema_version == 'spatial-ext' {
    case $ckan::ckan_version {
      '2.8': {
        $schema_content = 'ckan/ext/schema-2.8.xml.epp'
        $use_managed_schema = true
      }
      '2.7': {
        $schema_content = 'ckan/ext/schema-2.7.xml.epp'
        $use_managed_schema = true
      }
      '2.6': {
        $schema_content = 'ckan/ext/schema-2.6.xml.epp'
        $use_managed_schema = true
      }
      default: {
        $schema_content = 'ckan/ext/schema.xml.epp'
        $use_managed_schema = false
      }
    }

    if $use_managed_schema {
      ini_setting { 'managed_schema':
        ensure  => present,
        path    => "${ckan::ckan_src}/ckan/lib/search/__init__.py",
        setting => 'SOLR_SCHEMA_FILE_OFFSET',
        value   => '\'/admin/file/?file=managed-schema\'',
        require => File['/usr/lib/ckan/default/src'],
        before  => Class['solr'],
      }
    }

    # download file
    file { "${ckan::ckan_default}/schema.xml":
      ensure  => file,
      content => epp($schema_content, {
          solr_schema_version      => $ckan::solr_schema_version,
          solr_version_7_plus      => $ckan::solr_version_7_plus,
          solr_schema_extra_fields => $ckan::solr_schema_extra_fields,
      }),
      require => File['/usr/lib/ckan/default/src'],
      before  => Class['solr'],
    }
    $solr_schema_path = "${ckan::ckan_default}/schema.xml"
  } else {
    $solr_schema_path = "${ckan::ckan_src}/ckan/config/solr/schema-${ckan::solr_schema_version}.xml"
  }

  # check versions

  # Install CKAN deps
  if $ckan::version {
    if versioncmp($ckan::version, '2.9') <= 0 {
      $required_packages = $ckan::required_packages
    } else {
      $required_packages = $ckan::required_packages_2_9
    }
  } else {
    if versioncmp($ckan::source_version, '2.9') <= 0 {
      $required_packages = $ckan::required_packages
    } else {
      $required_packages = $ckan::required_packages_2_9
    }
  }

  ensure_packages($required_packages)

  # redis is a requirement
  class { 'ckan::redis':
    require => Package[$required_packages],
  }

  # add configuration directories
  file { [$ckan::ckan_etc, $ckan::ckan_default]:
    ensure  => directory,
    require => Class['ckan::redis'],
  }

  class { 'ckan::install::nginx':
    require => File[$ckan::ckan_etc, $ckan::ckan_default],
  }
  contain ckan::install::nginx

  class { 'ckan::install::apache':
    require => File[$ckan::ckan_etc, $ckan::ckan_default],
  }
  contain ckan::install::apache

  #exec { 'a2enmod wsgi':
  #  command => $ckan::wsgi_command,
  #  creates => $ckan::wsgi_creates,
  #  require => Class['ckan::install::apache'],
  #}

  # Install CKAN either from APT repositories or from the specified file
  if $ckan::install_from_source {
    file { ['/usr/lib/ckan','/usr/lib/ckan/default']:
      ensure  => directory,
      require => Class['ckan::install::apache'],
    }
    file { '/usr/local/bin/ckan_install_from_src.bash':
      ensure  => file,
      mode    => '0755',
      content => epp('ckan/scripts/ckan_install_from_src.bash.epp', { python_version=> $ckan::python_version }),
      require => File['/usr/lib/ckan','/usr/lib/ckan/default'],
    }
    exec { 'install_ckan':
      command => "/usr/local/bin/ckan_install_from_src.bash ${ckan::source_version}",
      creates => '/usr/lib/ckan/default/bin',
      timeout => 0,
      require => File['/usr/local/bin/ckan_install_from_src.bash'],
    }
    file { '/etc/ckan/default/who.ini':
      ensure  => link,
      target  => '/usr/lib/ckan/default/src/ckan/who.ini',
      require => Exec['install_ckan'],
      before  => [Class['ckan::install::nodejs','postgis','solr'], File['/usr/lib/ckan/default/src']],
    }
    if $ckan::install_datapusher {
      class { 'ckan::install::datapusher':
        require => File['/etc/ckan/default/who.ini'],
      }
      contain ckan::install::datapusher
    } elsif $ckan::install_datapusher_plus {
      class { 'ckan::install::datapusher_plus':
        require => File['/etc/ckan/default/who.ini'],
      }
      contain ckan::install::datapusher_plus
    }
  } else {
    if $ckan::is_ckan_from_repo == true {
      package { 'python-ckan':
        ensure  => latest,
        require => Class['ckan::install::apache'],
        before  => [
          Class['ckan::install::nodejs','postgis','solr'],
          File['/usr/lib/ckan/default/src','/etc/apache2/sites-enabled/ckan_default.conf', '/etc/apache2/sites-enabled/datapusher.conf']
        ],
      }
    } else {
      file { $ckan::ckan_package_dir:
        ensure  => directory,
        require => Class['ckan::install::apache'],
      }
      archive { "${ckan::ckan_package_dir}/${ckan_package_filename}":
        source   => $ckan_package_url,
        username => $ckan::ckan_package_url_user,
        password => $ckan::ckan_package_url_pass,
        require  => File[$ckan::ckan_package_dir],
      }
      package { 'python-ckan':
        ensure   => latest,
        provider => dpkg,
        source   => "${ckan::ckan_package_dir}/${ckan_package_filename}",
        require  => Archive["${ckan::ckan_package_dir}/${ckan_package_filename}"],
        before   => [
          Class['ckan::install::nodejs','postgis','solr'],
          File['/usr/lib/ckan/default/src','/etc/apache2/sites-enabled/ckan_default.conf', '/etc/apache2/sites-enabled/datapusher.conf']
        ],
      }
    }
    file { ['/etc/apache2/sites-enabled/ckan_default.conf', '/etc/apache2/sites-enabled/datapusher.conf']:
      ensure  => absent,
      require => Class['ckan::install::apache'],
    }
  }

  file { '/usr/lib/ckan/default/src':
    ensure  => directory,
    mode    => '0777',
    require => Class['ckan::install::apache'],
  }

  # dependancy cycle with nodejs and postgis
  class { 'ckan::install::nodejs':
    require => Class['ckan::install::apache'],
  }
  contain ckan::install::nodejs

  class { 'postgis':
    postgresql_version   => $ckan::postgresql_version,
    postgis_version      => $ckan::postgis_version,
    postgres_pass        => $ckan::postgres_pass,
    pg_hba_conf_defaults => $ckan::pg_hba_conf_defaults,
    pg_hba_rules         => $ckan::pg_hba_rules,
    require              => Class['ckan::install::apache'],
  }
  contain postgis

  # Install Solr/Jetty
  class { 'solr':
    url              => $ckan::solr_url,
    url_user         => $ckan::solr_url_user,
    url_pass         => $ckan::solr_url_pass,
    version          => $ckan::solr_version,
    solr_environment => $ckan::solr_environment,
    require          => Class['ckan::install::apache'],
  }
  contain solr

  # Jetty configuration
  # Note, internally to core, it notifies class solor so the require is
  # not necessary.
  solr::core { 'ckan':
    schema_src_file  => $solr_schema_path,
    elevate_src_file => '/opt/solr/example/files/conf/elevate.xml',
  }

  # Disable status module in apache, because its proxied from
  # nginx thus "require local" has no effect
  file { '/etc/apache2/mods-enabled/status.conf':
    ensure => 'absent',
    notify => Service[$ckan::apache_service],
  }
  file { '/etc/apache2/mods-enabled/status.load':
    ensure => 'absent',
    notify => Service[$ckan::apache_service],
  }
}
