# @summary A type which can be used to install a CKAN extension in the default
#          location.
#
# The ext directory classes utilize the ckan modularization framework known as
# [extensions](http://extensions.ckan.org).
# To fully install the plugin, puppet needs to be run twice.
# The first time puppet runs,
# the extension won't be added to ckan due to how this module manages the
# plugins.
#
# @param extname
#   The name of the extension. Defaults to $title.
#
# @param provider
#   The name of the VCS repository provider where the extension is hosted. Can
#   be any provider supported by puppetlabs/vcsrepo. Defaults to 'git'.
#
# @param source
#   The URL of the remote VCS repository. Defaults to
#   "http://github.com/ckan/ckanext-$extname".
#
# @param revision
#   The revision of the VCS repository to check out and install.
#
# @param branch
#   The branch of the VCS repository to check out and install.
#
# @param plugin
#   An array of strings that are plugins to add to the
#   plugin configuration in the production.ini.
#
# @param views
#   An array of string that are views to add to the
#   view configuration in the production.ini.
#
# @param pip_requirements
#   The name of the requirements pip file to be installed.
#   Only the name of the file which is found in the root directory of ext.
#
# @param user
#   Uses the user's $HOME/.ssh setup
#
# @param run_setup
#   Many extensions require the python setup.py develop command to be issued.
#   For convience, this has been added as a default feature which has been disabled.
#   Simply set this to true to run the setup.py command.
#
# @param run_setup_param
#   The paramter for the setup.py command.
#
define ckan::ext (
  Optional[String]        $extname          = undef,
  String                  $provider         = 'git',
  Optional[String]        $source           = undef,
  String                  $revision         = 'master',
  String                  $branch           = 'master',
  Optional[Array[String]] $plugin           = undef,
  Optional[Array[String]] $views            = undef,
  String                  $pip_requirements = 'pip-requirements.txt',
  Optional[String]        $user             = undef,
  Boolean                 $run_setup        = false,
  String                  $run_setup_param  = 'develop'
) {
  if ! defined(Class['ckan']) {
    fail( 'You must include the ckan base class before using any ckan defined resources')
  }

  $pip      = $ckan::pip
  $python   = $ckan::python
  $activate = '/usr/local/bin/ckan_activate_exec.bash'

  if $extname == undef {
    $_extname = $title
  } else {
    $_extname = $extname
  }

  if $source == undef {
    $_source = "http://github.com/ckan/ckanext-${_extname}"
  } else {
    $_source = $source
  }

  $extdir = "/usr/lib/ckan/default/src/ckanext-${_extname}"

  $defaults = {
    # TODO add require to
    require => Class['ckan::conf::production'],
  }

  # check if the plugins need to be updated
  if $plugin != undef {
    # update plugins with ini
    ensure_resource(ckan::conf::plugin_setting,$plugin,$defaults)
  }

  if $views != undef {
    # update views with ini
    ensure_resource(ckan::conf::views_setting,$views,$defaults)
  }

  vcsrepo { $extdir:
    ensure   => 'latest',
    provider => $provider,
    source   => $_source,
    revision => $revision,
    branch   => $branch,
    user     => $user,
    require  => File['/usr/lib/ckan/default/src'],
  }

  exec { "install ckanext-${_extname}":
    command     => "${activate} ${pip} install -e '${extdir}'",
    refreshonly => true,
    subscribe   => Vcsrepo[$extdir],
  }

  exec { "install ckanext-${_extname} requirements":
    command     => "${activate} ${pip} install -r '${extdir}/${pip_requirements}'",
    cwd         => $extdir,
    onlyif      => "/usr/bin/test -e '${extdir}/${pip_requirements}'",
    refreshonly => true,
    subscribe   => Exec["install ckanext-${_extname}"],
  }

  if $run_setup {
    exec { "setup ckanext-${_extname}":
      command     => "${activate} ${python} setup.py ${run_setup_param}",
      cwd         => $extdir,
      refreshonly => true,
      subscribe   => Exec["install ckanext-${_extname} requirements"],
    }
  }
}
