# @summary Configuration supporting database for ckan
#
# @see http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
class ckan::db_config {
  $password_hash           = postgresql_password('ckan_default', $ckan::ckan_pass)
  $datastore_password_hash = postgresql_password('datastore_default', $ckan::ckan_pass)

  # === configure postgresql ===
  # create the database
  # also create the user/password to access the db
  # user gets all privs by default
  postgresql::server::role { 'ckan_default':
    password_hash => $password_hash,
  }
  postgresql::server::database { 'ckan_default':
    owner   => 'ckan_default',
    require => Postgresql::Server::Role['ckan_default'],
  }
  # create a seperate db for the datastore extension
  postgresql::server::database { 'datastore_default' :
    owner   => 'ckan_default',
    require => Postgresql::Server::Role['ckan_default'],
  }
  # create a ro user for datastore extension
  postgresql::server::role { 'datastore_default' :
    password_hash => $datastore_password_hash,
    require       => Postgresql::Server::Database['datastore_default'],
  }
  # grant privs for datastore user
  postgresql::server::database_grant { 'datastore_default' :
    privilege => 'CONNECT',
    db        => 'datastore_default',
    role      => 'datastore_default',
    require   => Postgresql::Server::Role['datastore_default'],
  }
  # create a ro user for datastore extension
  postgresql::server::role { 'datapusher' :
    password_hash => postgresql::postgresql_password('datapusher', $ckan::datapusher_plus_password),
  }
  ## create database accounts
  postgresql::server::database { 'datapusher' :
    owner   => 'datapusher',
    require => Postgresql::Server::Role['ckan_default'],
  }
  postgresql::server::database_grant { 'datapusher_default' :
    privilege => 'ALL',
    db        => 'datastore_default',
    role      => 'datapusher',
    require   => Postgresql::Server::Role['datastore_default'],
  }
  postgresql::server::database_grant { 'datapusher_grant' :
    privilege => 'ALL',
    db        => 'datapusher',
    role      => 'datapusher',
    require   => Postgresql::Server::Role['datastore_default'],
  }
}
