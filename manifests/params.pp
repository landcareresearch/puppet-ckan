# @summary The os specific parameters for ckan installs.
#
# GPL-3.0+
#
class ckan::params {
  # variables
  # due to issue, have to supply https://github.com/ckan/ckan/issues/2110
  # our own license file
  # additional variables imported from config
  $ckan_etc       = '/etc/ckan'
  $ckan_default   = "${ckan_etc}/default"
  $ckan_src       = '/usr/lib/ckan/default/src/ckan'
  $ckan_lang_dir  = "${ckan_src}/ckan/i18n"
  $ckan_lang_base = "${ckan_src}/ckan/public/base/i18n"
  $ckan_ext       = '/usr/lib/ckan/default/src'
  $ckan_img_dir   = "${ckan_src}/ckan/public/base/images"
  $ckan_css_dir   = "${ckan_src}/ckan/public/base/css"
  $license_file   = 'license.json'
  $ckan_conf      = "${ckan_default}/production.ini"
  #$paster         = '/usr/lib/ckan/default/bin/paster'
  $paster         = '/usr/bin/paster'

  # used for setting jts for spatial search
  $jts_base_url = 'https://repo1.maven.org/maven2/com/vividsolutions/jts'
  $jts_1_13     = "${jts_base_url}/1.13/jts-1.13.jar"
  $jts_1_12     = "${jts_base_url}/1.12/jts-1.12.jar"
  $jts_url      = $jts_1_13

  # OS Specific configuration
  case $facts['os']['family'] {
    'redhat': {
      $required_packages = [
        'epel-release',
        'httpd',
        'mod-wsgi',
        'nginx',
        'libpqxx-devel',
        'python-paste-script',
        'expect',
        'redis',
        'wget',
      ]
      # wsgi specific configuration
      $wsgi_command  = '/usr/sbin/a2enmod wsgi'
      $wsgi_creates  = '/etc/httpd/mods-enabled/wsgi.conf'
      $apache_server = 'httpd'
    }

    'debian':{
      $required_packages = [
        'libpq5',
        'python-pastescript',
        'python-future',
        'expect',
        'python-dev',
        'python',
        'python-pip',
        'python-virtualenv',
        'libpq-dev',
        'git-core',
        'software-properties-common',
        'build-essential',
        'libxslt1-dev',
        'libxml2-dev',
        'libffi-dev',
        'redis-server',
        'wget',
      ]

      $required_packages_2_9 = ['python3-dev','libpq-dev', 'python3-pip', 'python3-venv','python3-pastescript','git-core','redis-server']

      # wsgi specific configuration
      $wsgi_command   = '/usr/sbin/a2enmod wsgi'
      $wsgi_creates   = '/etc/apache2/mods-enabled/wsgi.conf'
      $apache_service = 'apache2'
    }

    default: {
      fail("Unsupported OS ${facts['os']['family']}.  Please use a debian or redhat based system")
    }
  }

  # path to the activate & run command command
  $activate_exec = '/usr/local/bin/ckan_activate_exec.bash'

  # Ubuntu specific
  if $facts['os']['name'] == 'Ubuntu' {
    case $facts['os']['release']['full'] {
      '16.04': {
        $python_path        = '/usr/lib/ckan/default/bin/python'
        $pip_path           = '/usr/lib/ckan/default/bin'
        $postgresql_version = '9.6'
        $postgis_version    = '2.4'
      }
      '18.04': {
        $python_path        = '/usr/bin'
        $pip_path           = '/usr/bin'
        $postgresql_version = '9.6'
        $postgis_version    = '2.4'
      }
      default: {
        $python_path        = '/usr/lib/ckan/default/bin'
        $pip_path           = '/usr/lib/ckan/default/bin'
        $postgresql_version = '9.6'
        $postgis_version    = '2.4'
      }
    }
  } else {
    $python_path        = '/usr/lib/ckan/default/bin'
    $pip_path           = '/usr/bin'
    $postgresql_version = '9.4'
    $postgis_version    = '2.2'
  }
}
