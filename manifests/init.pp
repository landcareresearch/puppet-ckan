# @summary Installs, configures, and manages ckan.
#
# Install Details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
# Additional features:
#
# * Database can be backed up to <backup_dir>/ckan_database.pg_dump determined
#   by the frequency selected.
#
# @param site_url
#   The url for the ckan site.
#
# @param site_title
#   The title of the web site.
#
# @param server_name
#   The name for the vhost.
#
# @param site_description
#   The description (found in header) of the web site.
#
# @param site_intro
#   The introduction on the landing page.
#
# @param site_about
#   Information on the about page.
#
# @param plugins
#   Contains the ckan plugins to be used by the installation.
#   Do not include the following plugins as they are defaults and are
#   already included.
#   DO NOT INCLUDE: ['stats','text_view','image_view','recline_view']
#   @example
#       plugins => ['text_preview','datastore','resource_proxy', 'datapusher']
#
# @param app_instance_id
#   The secret password for the app instance .
#   To generate a config file that contains a new password,
#   run the following command:
#   @example
#     'paster make-config ckan /etc/ckan/default/dev.ini'
#
# @param beaker_secret
#   The secret password for beaker.
#   To generate a config file that contains a new password, run
#   the following command.
#   @example
#       'paster make-config ckan /etc/ckan/default/dev.ini'
#
# @param site_logo
#   The source of the logo.  Should be specified as
#   puppet:///<your module>/<image>.png
#   Requires the png file extension.
#
# @param license
#   The source to the json license file.  Should be specified as
#   puppet:///<your module>/<license file> and maintained by your module
#
# @param ckan_version
#   Helps identify settings and configuration necessary between the different
#   version of ckan.
#   Valid formats:
#
#   * '2.2'
#   * '2.3'
#   * eq: 'x.y'
#
#   Note, ckan_package_url & ckan_package_filename are not set,
#   than the ckan version will use the package url from ckan.org and the
#   appropriate name.
#
# @param is_ckan_from_repo
#   A boolean to indicate if the ckan package should be installed through an
#   already configured repository setup outside of this module.
#   If using Ubuntu/Deb should be able to do "apt-get install python-ckan"
#   Its the same idea for yum and other package managers.
#
# @param ckan_package_url
#   If not using a repo, then this url needs to be
#   specified with the location to download the package.
#   Note, this is using dpkg so deb/ubuntu only.
#
# @param ckan_package_url_user
#   If the URL is password protected, the user name.
#
# @param ckan_package_url_pass
#   If the URL is password protected, the password.
#
# @param ckan_package_filename
#   The filename of the ckan package.
#
# @param custom_css
#   The source to a css file used for the ckan site.
#   This replaces the default main.css.  Should be specified as
#   puppet:///<your module>/<css filename> and maintained by your module.
#   Images used in the custom css should be set in custom_imgs.
#
# @param custom_imgs
#   An array of source for the images to be used by the css.
#   Should be specified as
#   ['puppet:///<your module>/<img1>','...']
#
# @param recaptcha_version
#   The version of recaptcha.
#   Valid options:
#
#     * 1 Older style with a red box.  The user enters text
#     * 2 Newer style that the user clicks on a checkbox (cleaner).
#
#   @see [recaptcha-version](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-recaptcha-version)
#
# @param recaptcha_publickey
#   The public key for recaptcha.
#   @see [ckan-recaptcha-publickey](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-recaptcha-publickey)
#
# @param recaptcha_privatekey
#   The private key for recaptcha.
#   @see [recaptcha-privatekey](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-recaptcha-privatekey)
#
# @param max_resource_size
#   The maximum in megabytes a resource upload can be.
#   @see [max-resource-size](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-max-resource-size)
#
# @param apache_timeout
#   Amount of time the server will wait for certain events before failing a request
#   @see [apache timeout](https://httpd.apache.org/docs/2.4/mod/core.html#timeout)
#
# @param nginx_max_resource_size
#   Sets the maximum allowed size of the client request body.
#   @see [max resource size](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)
#
# @param nginx_proxy_connect_timeout
#   Defines a timeout for establishing a connection with a proxied server.
#   @see [proxy connect timeout](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)
#
# @param nginx_proxy_send_timeout
#   Sets a timeout for transmitting a request to the proxied server.
#   @see [proxy send timeout](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)
#
# @param nginx_proxy_read_timeout
#   Defines a timeout for reading a response from the proxied server.
#   @see [proxy read timeout](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)
#
# @param nginx_client_max_body_size
#   Sets the maximum allowed size of the client request body.
#   @see [client max body size](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)
#
# @param nginx_send_timeout
#   Sets a timeout for transmitting a response to the client.
#   @see [send timeout](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)
#
# @param nginx_keepalive_timeout
#   The first parameter sets a timeout during which a keep-alive client connection will stay open on the server side.
#   @see [keep alive](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)
#
# @param max_image_size
#  The maximum in megabytes an image upload can be.
#  @see [max-image-size](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-max-image-size)
#
# @param max_file_size
#   This sets the upper file size limit for in-line previews.
#   @see [resource-proxy-max-file-size](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-resource-proxy-max-file-size)
#
# @param datapusher_formats
#   File formats that will be pushed to the DataStore by the DataPusher.
#   When adding or editing a resource which links to a file in one of these
#   formats, the DataPusher will automatically try to import its contents
#   to the DataStore.
#
# @param create_unowned_dataset
#   Allow the creation of datasets not owned by any organization.
#   @see https://ckan.readthedocs.io/en/2.9/maintaining/authorization.html#ckan-auth-create-unowned-dataset
#
# @param default_views
#   Defines the resource views that should be created by default when creating
#   or updating a dataset. From this list only the views that are relevant to a
#   particular resource format will be created. This is determined by each
#   individual view.
#
# @param text_formats
#   Formats used for the text preview.
#   @see http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-preview-text-formats
#
# @param json_formats
#   JSON based resource formats that will be rendered by the Text view plugin
#   @see http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-preview-json-formats
#
# @param xml_formats
#   XML based resource formats that will be rendered by the Text view plugin
#   @see http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-preview-xml-formats
#
# @param image_formats
#   Space-delimited list of image-based resource formats that will be
#   rendered by the Image view plugin
#   @see http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-preview-image-formats
#
# @param create_user_via_api
#   Allow new user accounts to be created via the API by anyone. When False only sysadmins are authorised.
#   @see http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-auth-create-user-via-api
#
# @param create_user_via_web
#   Allow new user accounts to be created via the Web.
#   @see http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-auth-create-user-via-web
#
# @param postgres_pass
#   The password for the postgres user of the database (admin user).
#
# @param ckan_pass
#   The password for the ckan user of the database.
#
# @param pg_hba_conf_defaults
#   True if use the default hbas and false to configure your own.
#   This module uses postgresql so this setting informs the postgresql module
#   that the hba's should be handled outside of this module.
#   Requires your own hba configuration.
#
# @param pg_hba_rules
#   This is a hash which uses create_resources to create the hba rules.
#   Optional.  The hba rules can also be defined outside of this class.
#
# @param postgresql_version
#   The version of postgresql to use.
#
# @param postgis_version
#   The version of postgis to use.
#
# @param install_ckanapi
#   Installs the ckan api if set to true.
#   Also installs a helper script in /usr/bin/ckan/ckanapi.bash which
#   launches ckanapi with the environment setup.
#
#   @see https://github.com/ckan/ckanapi
#
# @param ckan_api_version
#   The version of the ckan api to install.
#
# @param enable_backup
#   Backs up the database to /backup/ckan_database.pg_dump.
#
# @param backup_daily
#   If backups enabled, sets backups to either daily (true) or weekly (false).
#
# @param backup_dir
#   The location where backups are stored.
#
# @param solr_url
#   The base url for downloading solr.
#
# @param solr_url_user
#   If the URL is password protected, the user name.
#
# @param solr_url_pass
#   If the URL is password protected, the password.
#
# @param solr_download_user
#   The user for downloading solr.
#
# @param solr_download_pass
#   The user's password for downloading solr.
#
# @param solr_version
#   The version of solr to install.
#
# @param solr_schema_version
#   The version of the solr schema to use.
#   Valid options:
#
#   * '1.2'
#   * '1.3'
#   * '1.4'
#   * '2.0'
#   * 'spatial' - configures solar with the spatial extensions.
#                 Only supports bounding box.
#   * 'spatial-ext' - configures solar with the extended spatial extension.
#                     This allows for bounding box, point, and Polygon.
#   * 'default'
#
#   The options correspond to the following structure:
#
#   ```
#   /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema-<solr_schema_version>
#   ```
#
#   The only exception is default which means schema.xml (required as of
#   ckan 2.3).
#
# @param solr_schema_extra_fields
#   An array of strings to be included in the schema.
#
# @param solr_jts_url
#   The url to be used to download the jts library for solr spatial ext.
#   Only used if spatial-ext option is set.
#
# @param solr_environment
#   Bash style environment variables passed at the end of the solr server environment.
#
# @param locale_default
#  Use this to specify the locale (language of the text) displayed in the
#  CKAN Web UI.
#
# @param locales_offered
#  By default, all locales found in the ckan/i18n directory will be offered to the user.
#  To only offer a subset of these, list them under this option. The ordering of the locales is preserved when offered to the user.
#  @see https://docs.ckan.org/en/2.8/maintaining/configuration.html#ckan-locales-offered
#
# @param i18n_directory
#   By default, the locales are searched for in the ckan/i18n directory.
#   Use this option if you want to use another folder.
#
# @param ckan_storage_path
#  The location where files will be stored for the file store.
#  Note, this module handles creating the directory; however, ensure the
#  path leading up to the directory has already been created.
#
# @param display_timezone
#   By default, all datetimes are considered to be in the UTC timezone. 
#   Use this option to change the displayed dates on the frontend.
#   Internally, the dates are always saved as UTC.
#   This option only changes the way the dates are displayed. 
#   The valid values for this options 
#   [can be found at pytz](http://pytz.sourceforge.net/#helpers) 
#   Available from CKAN 2.5+ (has no effect on previous versions).
#
# @param feeds_author_name
#   This controls the feed author’s name. If unspecified, it’ll use 
#   ckan.site_id.
#   @see [feeds-author-name](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-author-name)
#
# @param feeds_author_link
#   This controls the feed author’s link. If unspecified, it’ll
#   use ckan.site_url.
#   @see [feeds-author-link](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-author-link)
#
# @param feeds_authority_name
#   The domain name or email address of the default publisher of the feeds
#   and elements. If unspecified, it’ll use ckan.site_url.
#   @see [feeds-authority-name](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-authority-name)
#
# @param feeds_date
#   A string representing the default date on which the authority_name is
#   owned by the publisher of the feed.
#   @see [feeds-date](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-date)
#
# @param smtp_server
#   The SMTP server to connect to when sending emails with optional port.
#   The format is <DN>:<PORT>
#   @example: smtp.example.com:587
#   @see [smtp-server](http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=email#smtp-server)
#
# @param smtp_starttls
#   Whether or not to use STARTTLS when connecting to the SMTP server.
#   @see [smtp-starttls](http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=email#smtp-starttls)
#
# @param smtp_user
#   The username used to authenticate with the SMTP server.
#   Example: username@example.com
#   @see [smtp-user](http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=email#smtp-user)
#
# @param smtp_password
#   The password used to authenticate with the SMTP server.
#   @see [smtp-password](http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=email#smtp-password)
#
# @param smtp_mail_from
#   The email address that emails sent by CKAN will come from.
#   Note that, if left blank, the SMTP server may insert its own.
#   Example: ckan@example.com
#   @see [smtp-mail-from](http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=email#smtp-mail-from)
#
# @param email_errors_to
#   An e-mail address to send any error reports to.
#   Example: errors@example.com
#   @see [email-errors-to](http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=email#email-to)
#
# @param email_errors_from
#   The from e-mail address to use when sending any error reports.
#   Example: ckan-errors@example.com
#   @see [email-errors-from](http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=email#error-email-from)
#
# @param roles_cascade
#   Makes role permissions apply to all the groups down the hierarchy from
#   the groups that the role is applied to.
#   @see [roles-cascade](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-auth-roles-that-cascade-to-sub-groups)
#
# @param featured_orgs
#   Defines a list of organization names or ids.
#   Example: 'org_one'
#   @see [featured-orgs](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-featured-orgs)
#
# @param featured_groups
#   Defines a list of group names or group ids.
#   Example: 'group_one'
#   @see [featured-groups](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-featured-groups)
#
# @param activity_streams_enabled
#   Turns on and off the activity streams used to track changes on
#   datasets, groups, users, etc
#   @see [activity-streams-enabled](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-activity-streams-enabled)
#
# @param activity_streams_email_notifications
#   Turns on and off the activity streams’ email notifications.
#   @see [activity-streams-email-notifications](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-activity-streams-email-notifications)
#
# @param tracking_enabled
#   This controls if CKAN will track the site usage.
#   @see [tracking-enabled](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-tracking-enabled)
#
# @param search_facets_default
#   Default number of facets shown in search results.
#   @see [search-facets-default](https://docs.ckan.org/en/latest/maintaining/configuration.html#search-facets-default)
#
# @param favicon
#   This sets the site’s favicon. 
#   @see http://docs.ckan.org/en/latest/maintaining/configuration.html?highlight=favicon#ckan-favicon
#
# @param use_ssl
#   Enable SSL for the site.
#
# @param use_google_verification
#   Enable google verification in the nginx configuration.
#
# @param use_nagios
#   Use a /nagios.txt in the nginx configuration file.
#
# @param google_verification_string
#   The google verification string.
#
# @param ssl_cert_path
#   The path to the ssl certificate for nginx.
#
# @param ssl_key_path
#   The path to the ssl key for nginx.
#
# @param disable_bot_pipl
#   Disable the piplbot
#   @see https://pipl.com/bot
#
# @param disable_all_bots
#   Diables all bots from indexing, archiving, etc.
#   @see https://docs.nwebsec.com/en/4.1/nwebsec/Configuring-xrt.html
#
# @param install_from_source
#   Install from source instead of package.
#
# @param source_version
#   The source version to install.
#
# @param datapusher_version
#   The version of datapusher to install if using the source version.  Ignored if using packaged version.
#   @see https://github.com/ckan/datapusher
#
# @param datapusher_plus_password
#   The password for the datapusher user in the database.
#
# @param install_datapusher
#   Controlls if the datapusher should be intalled.  Only applicable for the source install because the package install
#   automatically installs the datapusher.

# @param install_datapusher_plus
#   Controlls if the datapusher-plus should be intalled.
#
# @example Default Setup
#    class { 'ckan':
#      site_url              => 'test.ckan.com',
#      site_title            => 'CKAN Test',
#      site_description      => 'A shared environment for managing Data.',
#      site_intro            => 'A CKAN test installation',
#      site_about            => 'Pilot data catalogue and repository.',
#      plugins               =>
#       text_preview recline_preview datastore resource_proxy pdf_preview',
#      is_ckan_from_repo     => 'false',
#      ckan_package_url      =>
#       'http://packaging.ckan.org/python-ckan_2.1_amd64.deb',
#      ckan_package_filename => 'python-ckan_2.1_amd64.deb',
#    }
#
class ckan (
  String                  $site_url                             = 'localhost',
  String                  $site_title                           = 'localhost',
  String                  $server_name                          = undef,
  Optional[String]        $site_description                     = undef,
  Optional[String]        $site_intro                           = undef,
  Optional[String]        $site_about                           = undef,
  Optional[Array[String]] $plugins                              = undef,
  Optional[String]        $app_instance_id                      = undef,
  Optional[String]        $beaker_secret                        = undef,
  Optional[String]        $site_logo                            = undef,
  String                  $license                              = 'puppet:///modules/ckan/license.json',
  Boolean                 $is_ckan_from_repo                    = false,
  Optional[String]        $ckan_package_url                     = undef,
  Optional[String]        $ckan_package_url_user                = undef,
  Optional[String]        $ckan_package_url_pass                = undef,
  Optional[String]        $ckan_package_filename                = undef,
  Optional[String]        $ckan_version                         = undef,
  String                  $custom_css                           = 'main.css',
  Optional[Array[String]] $custom_imgs                          = undef,
  String                  $recaptcha_version                    = '2',
  Optional[String]        $recaptcha_publickey                  = undef,
  Optional[String]        $recaptcha_privatekey                 = undef,
  String                  $max_resource_size                    = '100',
  Optional[String]        $apache_timeout                       = undef,
  Optional[String]        $nginx_max_resource_size              = undef,
  Optional[String]        $nginx_proxy_connect_timeout          = undef,
  Optional[String]        $nginx_proxy_send_timeout             = undef,
  Optional[String]        $nginx_proxy_read_timeout             = undef,
  Optional[String]        $nginx_client_max_body_size           = undef,
  Optional[String]        $nginx_send_timeout                   = undef,
  Optional[String]        $nginx_keepalive_timeout              = undef,
  String                  $max_image_size                       = '10',
  String                  $max_file_size                        = '1048576',
  String                  $datapusher_formats                   = 'csv xls application/csv application/vnd.ms-excel',
  Boolean                 $create_unowned_dataset               = false,
  Array[String]           $default_views                        = ['image_view','recline_view'],
  String                  $text_formats                         = 'text plain text/plain',
  String                  $json_formats                         = 'json',
  String                  $xml_formats                          = 'xml rdf rss',
  String                  $image_formats                        = 'png jpeg jpg gif',
  String                  $create_user_via_api                  = 'false',
  String                  $create_user_via_web                  = 'false',
  String                  $postgres_pass                        = 'pass',
  String                  $ckan_pass                            = 'pass',
  Boolean                 $pg_hba_conf_defaults                 = true,
  Optional[Hash]          $pg_hba_rules                         = undef,
  String                  $postgresql_version                   = $ckan::params::postgresql_version,
  String                  $postgis_version                      = $ckan::params::postgis_version,
  Boolean                 $install_ckanapi                      = false,
  String                  $ckan_api_version                     = '3.3',
  Boolean                 $enable_backup                        = true,
  Boolean                 $backup_daily                         = true,
  String                  $backup_dir                           = '/backup',
  String                  $solr_url                             = 'http://archive.apache.org/dist/lucene/solr',
  Optional[String]        $solr_url_user                        = undef,
  Optional[String]        $solr_url_pass                        = undef,
  Optional[String]        $solr_download_user                   = undef,
  Optional[String]        $solr_download_pass                   = undef,
  String                  $solr_version                         = '5.5.3',
  String                  $solr_schema_version                  = 'default',
  Optional[Array]         $solr_schema_extra_fields             = undef,
  String                  $solr_jts_url                         = $ckan::params::jts_url,
  Optional[Array]         $solr_environment                     = undef,
  String                  $locale_default                       = 'en',
  Optional[String]        $locales_offered                      = undef,
  String                  $ckan_storage_path                    = '/var/lib/ckan/default',
  String                  $display_timezone                     = 'UTC',
  Optional[String]        $feeds_author_name                    = undef,
  Optional[String]        $feeds_author_link                    = undef,
  Optional[String]        $feeds_authority_name                 = undef,
  Optional[String]        $feeds_date                           = undef,
  Boolean                 $activity_streams_enabled             = true,
  Boolean                 $activity_streams_email_notifications = false,
  Boolean                 $tracking_enabled                     = false,
  Integer                 $search_facets_default                = 10,
  String                  $favicon                              = '/base/images/ckan.ico',
  Boolean                 $use_ssl                              = false,
  Boolean                 $use_google_verification              = false,
  Boolean                 $use_nagios                           = false,
  Optional[String]        $i18n_directory                       = undef,
  Optional[String]        $smtp_server                          = undef,
  Optional[String]        $smtp_starttls                        = undef,
  Optional[String]        $smtp_user                            = undef,
  Optional[String]        $smtp_password                        = undef,
  Optional[String]        $smtp_mail_from                       = undef,
  Optional[String]        $email_errors_to                      = undef,
  Optional[String]        $email_errors_from                    = undef,
  Optional[String]        $roles_cascade                        = undef,
  Optional[String]        $featured_orgs                        = undef,
  Optional[String]        $featured_groups                      = undef,
  Optional[String]        $google_verification_string           = undef,
  Optional[String]        $ssl_cert_path                        = undef,
  Optional[String]        $ssl_key_path                         = undef,
  Boolean                 $disable_bot_pipl                     = false,
  Boolean                 $disable_all_bots                     = false,
  Boolean                 $install_from_source                  = false,
  String                  $source_version                       = '2.7.7',
  String                  $datapusher_version                   = '0.0.16',
  String                  $datapusher_plus_password             = 'datapusher-pass',
  Boolean                 $install_datapusher                   = true,
  Boolean                 $install_datapusher_plus              = false,
) inherits ckan::params {
  # Check supported operating systems
  if $facts['os']['family'] != 'Debian' {
    fail("Unsupported OS ${facts['os']['family']}.  Please use a debian based system")
  }

  File {
    owner => root,
    group => root,
  }

  $ckan_package_dir = '/usr/local/ckan'

  $default_views_string = join($default_views,' ')

  if versioncmp($solr_version, '6.6.0') >= 0 {
    $solr_version_7_plus = true
  } else {
    $solr_version_7_plus = false
  }

  if $ckan::version {
    if versioncmp($ckan::version, '2.9') <= 0 {
      $pip            = "${ckan::params::pip_path}/pip"
      $python         = "${ckan::params::python_path}/python"
      $python_version = 2
    } else {
      $pip            = "${ckan::params::pip_path}/pip3"
      $python         = "${ckan::params::python_path}/python3"
      $python_version = 3
    }
  } else {
    if versioncmp($ckan::source_version, '2.9') <= 0 {
      $pip            = "${ckan::params::pip_path}/pip"
      $python         = "${ckan::params::python_path}/python"
      $python_version = 2
    } else {
      $pip            = "${ckan::params::pip_path}/pip3"
      $python         = "${ckan::params::python_path}/python3"
      $python_version = 3
    }
  }

  contain ckan::install

  class { 'ckan::db_config':
    require => Class['ckan::install'],
  }

  class { 'ckan::config':
    backup_dir => $backup_dir,
    site_logo  => $site_logo,
    require    => Class['ckan::db_config'],
  }

  class { 'ckan::conf::production':
    require => Class['ckan::config'],
  }

  if $install_from_source {
    class { 'ckan::supervisor':
      require => Class['ckan::conf::production'],
      before  => Class['ckan::service'],
    }
  }

  class { 'ckan::service':
    subscribe => Class['ckan::config','ckan::db_config', 'ckan::conf::production'],
  }

  class { 'ckan::postinstall':
    require => Class['ckan::service'],
  }
  if $install_ckanapi {
    class { 'ckan::ckanapi':
      require => Class['ckan::postinstall'],
    }
    contain ckan::ckanapi
  }

  #contain ckan::nginx_config
  contain ckan::db_config
  contain ckan::config
  contain ckan::conf::production
  contain ckan::service
  contain ckan::postinstall
}
