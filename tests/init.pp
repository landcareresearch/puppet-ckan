### set locale
class { 'locales':
  default_locale => 'en_US.UTF-8',
  lc_all         => 'en_US.UTF-8',
  locales        => ['en_US.UTF-8 UTF-8'],
}

$plugins             = ['text_preview','datastore','resource_proxy', 'datapusher']
$datapusher_formats  =
  'csv xls xlsx application/csv application/vnd.ms-excelapplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

class { 'ckan':
  site_url             => 'http://192.168.33.10',
  server_name          => 'localhost',
  site_title           => 'CKAN Test',
  site_description     => 'A shared environment for managing Data.',
  site_intro           => 'A CKAN test installation',
  site_about           => 'Pilot data catalogue and repository.',
  plugins              => $plugins,
  app_instance_id      => 'pass',
  beaker_secret        => 'pass',
  is_ckan_from_repo    => false,
  max_resource_size    => '1000',
  max_image_size       => '15',
  datapusher_formats   => $datapusher_formats,
  pg_hba_conf_defaults => true,
  install_ckanapi      => true,
  ckan_version         => '2.8',
  source_version       => '2.8.6',
  install_from_source  => true,
  solr_version         => '6.5.1',
  #solr_schema_version  => 'default',
  solr_schema_version  => 'spatial',
  display_timezone     => 'Pacific/Auckland',
  require              => Class['locales'],

  # for testing
#  locale_default       => 'en_LRNZ',
}

# debuggin issue 5015
class { 'ckan::ext::geoview':
  openlayers_formats => 'wms wfs geojson gml kml',
}
#class{'ckan::ext::geoview':
#  openlayers_formats            => 'wms wfs geojson gml kml',
#  openlayers_viewer_enable      => false,
#  leaflet_geojson_viewer_enable => true,
#  leaflet_wmts_viewer_enable    => false,
#  openlayers_hoveron            => false,
#  openlayers_hide_overlays      => false,
#}

#ckan::lang{'en_LRNZ':
#  url => 'https://bitbucket.org/landcareresearch/ckanext-en-lrnz/downloads/en_LRNZ-2.6.2.zip'
#}

#class{'ckan::ext::lcrnz':
#  uri      => 'ldap://openldap.landcareresearch.co.nz:389',
#  base_dn  => 'ou=users,dc=landcareresearch,dc=co,dc=nz',
#  source   => 'http://bitbucket.org/landcareresearch/ckanext-lcrnz',
#  revision => 'master',
#  notify   => Service[$::ckan::apache_service, 'nginx'],
#}

#class {'ckan::ext::spatial':
#  template       => 'lcrnz',
#  map_provider   => 'openstreetmap',
#  default_extent => '[[-49.43,160.74], [-32.43,185.91]]',
#}

#contain ckan::ext::harvest
#contain ckan::ext::dcat
#contain ckan::ext::officedocs
#contain ckan::ext::zippreview

# test for the new galleries plugin

#class {'ckan::ext::lcrnz':
#  uri             => 'ldap://openldap.landcareresearch.co.nz:389',
#  organization_id => '',
#  base_dn         => 'ou=users,dc=landcareresearch,dc=co,dc=nz',
#}

#class {'ckan::ext::spatial':
#  template => 'lcrnz',
#}

#class {'ckan::ext::hierarchy':}
#class {'ckan::ext::ldap': uri => 'localhost', base_dn => 'dc=local,dc=com'}
#class {'ckan::ext::pages':}
#class {'ckan::ext::pdfview': }
#class {'ckan::ext::private_datasets': }
#class {'ckan::ext::repeating': }
#class {'ckan::ext::showcase': }
#class {'ckan::ext::viewhelpers': }

# requires archiver
#class {'ckan::ext::geoview':}

# extensions with problems
#class {'ckan::ext::googleanalytics':
#  id       => 'id',
#  account  => 'account',
#  username => 'user',
#  password => 'pass',
#}
#class {'ckan::ext::archiver':}
#class {'ckan::ext::dcat': }
#class {'ckan::ext::galleries': }
#class {'ckan::ext::harvest':}
#class {'ckan::ext::report': }
#class {'ckan::ext::officedocs':}
