# Project Contributors

## [Michael Speth](mailto:spethm@landcareresearch.co.nz?subject=[Bitbucket]%20Puppet%20CKAN)

* Lead Developer

## [Nick Stenning](mailto:nick@whiteink.com?subject=[Bitbucket]%20Puppet%20CKAN)

* Contributor

## [Craig Miskell](mailto:craig@catalyst.net.nz?subject=[Bitbucket]%20Puppet%20CKAN)

* Contributor
